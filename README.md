# Learning ASL

Learning ASL is a [Unity](https://unity.com/) app which utilizes the [Leap Motion Controller](https://www.ultraleap.com/product/leap-motion-controller/) and a Recurrent Neural Network to detect American Sign Language Letters in real time.  
The RNN was trained with ~300 samples per sign using the TensorFlow python API and then imported into Unity. 
This application was developed by me for a research project for my University.  



The application allows:

* Real time inference of alphabetical hand signs
* Viewing the networks inference confidence 
* Adjusting the networks confidence tolerance for each individual sign 
* Recording hand data, which is stored in CSV format
* Adjusting the camera count-down timer 
* Enabling the raw camera feed of the Leap Motion Controller 
* Enabling sign demo videos or images for each individual sign

![Inference Demo](readmemedia/nn_demo.mp4)

---

## Installation Requirements:
#### Hardware:
- Leap Motion Controller
#### Software: 
- Unity 2018.4.22f1 
- TFSharpPlugin 

## Installation Steps:
1. Install Unity 2018.4.22f1
2. Clone the repository 
3. Open project in Unity editor 
4. Download and install [TFSharpPlugin](https://s3.amazonaws.com/unity-ml-agents/0.5/TFSharpPlugin.unitypackage) 
5. Build & run

---

![Recording Demo](readmemedia/rec_demo.mp4)

## Interface:
### Menu:
When starting the application you will be greeted with two options:
* "Recognize Signs" for inference
* "Add Sign Data" for recording signs

Select one of the two to continue 
### Settings:
Access settings in the top left corner:
* Enable raw camera feed 
* Switch between low and high detail hand model
* Switch between picture and video instructions 
* Switch between automatic inference and manual inference (requires pressing of spacebar)
* Enable outline of lead hand 
### Infering Signs:
* Clicking the blue exclamation mark below the settings button will display the networks inference confidence 
### Recording Signs:
* Select alphabet sign by pressing keyboard key 
* Press space bar to record single snapshot of tracked hand
* Hold space bar to record a sequence of the tracked hand 
* Both Image and Leap data will be stored and can be viewed at `compiledProjectDir/ASL_Data/Data/` 
