﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.Linq;
using UnityEngine;
using Leap;
using Leap.Unity;

public class ImageRetriever : MonoBehaviour
{
    public const int IMAGE_WARNING_WAIT = 10;
    public const int LEFT_IMAGE_INDEX = 0;
    public const int RIGHT_IMAGE_INDEX = 1;
    public const float IMAGE_SETTING_POLL_RATE = 2.0f;

    //[SerializeField]
    //[FormerlySerializedAs("gammaCorrection")]
    private float _gammaCorrection = 1.0f;

    private LeapServiceProvider leapProvider;
    //private EyeTextureData _eyeTextureData = new Leap.UnityEyeTextureData();

    protected ProduceConsumeBuffer<Leap.Image> _imageQueue = new ProduceConsumeBuffer<Leap.Image>(32);
    protected Leap.Image _currentImage = null;

    private Texture2D reconstructedCurrentImage = null;

    private LeapTextureData TextureData = new LeapTextureData();
    private LeapDistortionData DistortionData = new LeapDistortionData();



    public Texture2D getCurrentImageTexture()
    {
        return reconstructedCurrentImage;
    }



    void Awake()
    {
        leapProvider = GetComponent<LeapServiceProvider>();
        if (leapProvider == null)
        {
            leapProvider = GetComponentInChildren<LeapServiceProvider>();
        }

        //Enable pooling to reduce overhead of images
        LeapInternal.MemoryManager.EnablePooling = true;

        ApplyGammaCorrectionValues();
    }

    private void OnEnable()
    {
        subscribeToService();
    }

    private void OnDisable()
    {
        unsubscribeFromService();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        Controller controller = leapProvider.GetLeapController();
        if (controller != null)
        {
            leapProvider.GetLeapController().DistortionChange -= onDistortionChange;
        }
    }

    private void LateUpdate()
    {
        Frame imageFrame = leapProvider.CurrentFrame;

        _currentImage = null;

        /* Use the most recent image that is not newer than the current frame
         * This means that the shown image might be slightly older than the current
         * frame if for some reason a frame arrived before an image did.
         * 
         * Usually however, this is just important when robust mode is enabled.
         * At that time, image ids never line up with tracking ids.
         */
        Image potentialImage;
        while (_imageQueue.TryPeek(out potentialImage))
        {
            if (potentialImage.SequenceId > imageFrame.Id)
            {
                break;
            }
            _currentImage = potentialImage;
            _imageQueue.TryDequeue();
        }
        if (_currentImage != null)
        {
            if (CheckStale(_currentImage))
            {
                TextureData.reconstructImage(_currentImage);
                reconstructedCurrentImage = TextureData.getImageTexture();
            }
        }


    }

    private void subscribeToService()
    {
        if (_serviceCoroutine != null)
        {
            return;
        }

        _serviceCoroutine = StartCoroutine(serviceCoroutine());
    }

    private void unsubscribeFromService()
    {
        if (_serviceCoroutine != null)
        {
            StopCoroutine(_serviceCoroutine);
            _serviceCoroutine = null;
        }

        var controller = leapProvider.GetLeapController();
        if (controller != null)
        {
            controller.ClearPolicy(Controller.PolicyFlag.POLICY_IMAGES);
            controller.ImageReady -= onImageReady;
            controller.DistortionChange -= onDistortionChange;
        }
    }

    private Coroutine _serviceCoroutine = null;
    private IEnumerator serviceCoroutine()
    {
        Controller controller = null;
        do
        {
            controller = leapProvider.GetLeapController();
            yield return null;
        } while (controller == null);

        controller.SetPolicy(Controller.PolicyFlag.POLICY_IMAGES);
        controller.ImageReady += onImageReady;
        controller.DistortionChange += onDistortionChange;
    }

    private void onImageReady(object sender, ImageEventArgs args)
    {
        Image image = args.image;
        _imageQueue.TryEnqueue(image);
    }

    public void ApplyGammaCorrectionValues()
    {
        float gamma = 1f;
        if (QualitySettings.activeColorSpace != ColorSpace.Linear)
        {
            gamma = -Mathf.Log10(Mathf.GammaToLinearSpace(0.1f));
        }
        //Shader.SetGlobalFloat(GLOBAL_COLOR_SPACE_GAMMA_NAME, gamma);
        //Shader.SetGlobalFloat(GLOBAL_GAMMA_CORRECTION_EXPONENT_NAME, 1.0f / _gammaCorrection);
    }

    void onDistortionChange(object sender, LeapEventArgs args)
    {
        //_eyeTextureData.MarkStale();
    }

    public bool CheckStale(Image image)
    {
        bool _isStale = false;
        return TextureData.CheckStale(image) ||
               DistortionData.CheckStale() ||
               _isStale;
    }

    public class LeapTextureData
    {
        private Texture2D _combinedTexture = null;
        private byte[] _intermediateArray = null;

        public void reconstructImage(Image image)
        {
            int combinedWidth = image.Width;
            int combinedHeight = image.Height;// * 2; //figure out why?

            int bytesPerPixel = image.BytesPerPixel;

            byte[] rawImageData = image.Data(0); //Get Left Camera Output

            float[] distortionBuffer = image.Distortion(0); //Get Left Camera Distortion 

            int rawImageDataLength = rawImageData.Length;

            TextureFormat format = getTextureFormat(image); //get Image format (should be alpha8)

            
            for (int i = 0; i < rawImageData.Length / 2; i++)  //flip Image
            {
                byte tmp = rawImageData[i];
                rawImageData[i] = rawImageData[rawImageDataLength - i - 1];
                rawImageData[rawImageDataLength - i - 1] = tmp;
            }

            //Debug.Log(rawImageData.Max());

            _combinedTexture = null; //Reset Texture 

            _combinedTexture = new Texture2D(combinedWidth, combinedHeight, format, false, true);
            _combinedTexture.wrapMode = TextureWrapMode.Clamp;
            _combinedTexture.filterMode = FilterMode.Bilinear;
            _combinedTexture.LoadRawTextureData(rawImageData); //..this will auto-resize the texture dimensions.
            _combinedTexture.Apply();

        }

        public Texture2D getImageTexture()
        {
            return _combinedTexture;
        }

        public bool CheckStale(Image image)
        {
            if (_combinedTexture == null || _intermediateArray == null)
            {
                return true;
            }

            if (image.Width != _combinedTexture.width || image.Height * 2 != _combinedTexture.height)
            {
                return true;
            }

            if (_combinedTexture.format != getTextureFormat(image))
            {
                return true;
            }

            return false;
        }

        private TextureFormat getTextureFormat(Image image)
        {
            switch (image.Format)
            {
                case Image.FormatType.INFRARED:
                    return TextureFormat.Alpha8;
                default:
                    throw new Exception("Unexpected image format " + image.Format + "!");
            }
        }
    }

    public class LeapDistortionData
    {
        private Texture2D _combinedTexture = null;

        public bool CheckStale()
        {
            return _combinedTexture == null;
        }
    }


}
