﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TensorFlow;
using System.Text;

public class LeapSeqNNv2 : MonoBehaviour
{
    public TextAsset graphModel;
    private byte[] graphModelBytes;

    private const int input_length = 60;   //neural network input size  
    private const int output_length = 26;   //neural network output size 
    private const int sequence_length = 60;
    private const int new_sample_index = sequence_length - 1;

    private float[,,] inputTensor = new float[1, sequence_length, input_length];
    private float[] outputArray = new float[output_length];

    private float[] magnitudeArray = new float[(input_length - 3) / 3];
    //private int chosen_sign;
    private bool newPrediction = false;

    private bool automaticPrediction = false;

    private bool LeapIsRunning = false;

    private bool newData = false;


    // Start is called before the first frame update
    void Start()
    {
        graphModelBytes = graphModel.bytes;

        resetNNInput();
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */

    public void resetNNInput()
    {
        for (int i = 0; i < sequence_length; i++)
        {
            for (int j = 0; j < input_length; j++)
            {
                inputTensor[0, i, j] = 0.0f;
            }
        }
    }

    private float[] evaluate()
    {
        float[,] outputTensor;

        using (var graph = new TFGraph())
        {
            graph.Import(graphModelBytes);
            var session = new TFSession(graph);
            var runner = session.GetRunner();

            //debugInputTensor();

            TFTensor input = inputTensor;

            runner.AddInput(graph["input"][0], input);

            runner.Fetch(graph["output"][0]);

            outputTensor = runner.Run()[0].GetValue() as float[,];
            // frees up resources - very important if you are running graph > 400 or so times
            session.Dispose();
            graph.Dispose();
        }

        newData = false;

        float[] outputArray = new float[outputTensor.GetLength(1)];

        for (int i = 0; i < outputTensor.GetLength(1); i++)
        {
            outputArray[i] = outputTensor[0, i];
        }

        return outputArray;
    }

 
    public void continuousPrediction()
    {
        LeapIsRunning = true;
        while (automaticPrediction)
        {
            if (newData)
            {
                evaluate();
            }

        }
    }

    public void makePrediction()
    {
        outputArray = evaluate();
        newPrediction = true;
    }

    public float[] getPrediction()
    {
        newPrediction = false;
        return outputArray;
    }

    public bool isAutomaticPredicition()
    {
        return automaticPrediction;
    }

    public bool isRunning()
    {
        return LeapIsRunning;
    }

    public void endAutomaticPredicition()
    {
        LeapIsRunning = false;
    }

    public bool predicitionAvailable()
    {
        return newPrediction;
    }

    public bool needsNewData()
    {
        return !newData;
    }

    public void setData()
    {
        newData = true;
    }

    public void updateLeapData(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        for (int i = 1; i < sequence_length; i++)
        {
            for (int j = 0; j < input_length; j++)
            {
                inputTensor[0, i - 1, j] = inputTensor[0, i, j];
            }
        }

        convertToVectors(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
        getMagnitudes();    //get magnitude for each vector in input array
        inputToNormalVectors(); //convert input vectors to normal vectors
        newData = true;
    }


    private static void rotateX(ref float x, ref float y, ref float z, ref float angle)
    {
        float cosAngle = (float)Math.Cos(angle);
        float sinAngle = (float)Math.Sin(angle);
        float _y = y * cosAngle - z * sinAngle;
        z = y * sinAngle + z * cosAngle;
        y = _y;
    }

    private static void rotateY(ref float x, ref float y, ref float z, ref float angle)
    {
        float cosAngle = (float)Math.Cos(angle);
        float sinAngle = (float)Math.Sin(angle);
        float _x = x * cosAngle + z * sinAngle;
        z = z * cosAngle - x * sinAngle;
        x = _x;
    }

    private static void rotateZ(ref float x, ref float y, ref float z, ref float angle)
    {
        float cosAngle = (float)Math.Cos(angle);
        float sinAngle = (float)Math.Sin(angle);
        float _x = x * cosAngle - y * sinAngle;
        y = x * sinAngle + y * cosAngle;
        x = _x;
    }


    private void convertToVectors(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        int inputIndex = 0;
        bool overwroteThumbBone = false;

        for (int i = 0; i < fingerTipMatrix.Length; i++)
        {
            for (int j = 0; j < fingerTipMatrix[i].Length; j++)
            {

                inputTensor[0, new_sample_index, inputIndex] =      fingerTipMatrix[i][j][0] - fingerBaseMatrix[i][j][0];
                inputTensor[0, new_sample_index, inputIndex + 1] =  fingerTipMatrix[i][j][1] - fingerBaseMatrix[i][j][1];
                inputTensor[0, new_sample_index, inputIndex + 2] =  fingerTipMatrix[i][j][2] - fingerBaseMatrix[i][j][2];

                inputIndex += 3;
                if (inputIndex == 12 && !overwroteThumbBone)
                {
                    inputIndex = 9;
                    overwroteThumbBone = true;
                }
            }
        }

        //Debug.Log(inputIndex);
        for (int i = 0; i < 3; i++)
        {
            inputTensor[0, new_sample_index, inputIndex] = handOrientation[i];
            inputIndex += 1;
        }

    }

    private void getMagnitudes()
    {
        for (int i = 0; i < (input_length - 3) / 3; i++)
        {
            magnitudeArray[i] = (float)Math.Sqrt(Math.Pow(inputTensor[0, new_sample_index, i * 3], 2) + Math.Pow(inputTensor[0, new_sample_index, i * 3 + 1], 2) + Math.Pow(inputTensor[0, new_sample_index, i * 3 + 2], 2));
        }
    }

    private void inputToNormalVectors()
    {
        float xPitchAngle = inputTensor[0, new_sample_index, input_length - 3];
        float yYawAngle =   inputTensor[0, new_sample_index, input_length - 2];
        float zRollAngle =  inputTensor[0, new_sample_index, input_length - 1];

        for (int i = 0; i < (input_length - 3) / 3; i++)
        {
            inputTensor[0, new_sample_index, i * 3] = inputTensor[0, new_sample_index, i * 3] / magnitudeArray[i];
            inputTensor[0, new_sample_index, i * 3 + 1] = inputTensor[0, new_sample_index, i * 3 + 1] / magnitudeArray[i];
            inputTensor[0, new_sample_index, i * 3 + 2] = inputTensor[0, new_sample_index, i * 3 + 2] / magnitudeArray[i];

            rotateX(ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref xPitchAngle);
            rotateY(ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref yYawAngle);
            rotateZ(ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref inputTensor[0, new_sample_index, i * 3], ref zRollAngle);
        }

        //inputTensor[0, new_sample_index, 3 * 3] = 0.0F;
        //inputTensor[0, new_sample_index, 3 * 3 + 1] = 0.0F;
        //inputTensor[0, new_sample_index, 3 * 3 + 2] = 0.0F;

    }




    private void fillInputTensor(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        int index = 0;
        flattenToInput(ref fingerTipMatrix, ref index);
        //Debug.Log(index);
    }

    private void flattenToInput(ref float[][][] matrix, ref int index)
    {
        for (int i = 0; i < matrix.Length; i++)
        {
            flattenToInput(ref matrix[i], ref index);
        }
    }

    private void flattenToInput(ref float[][] matrix, ref int index)
    {
        for (int i = 0; i < matrix.Length; i++)
        {
            flattenToInput(ref matrix[i], ref index);
        }
    }

    private void flattenToInput(ref float[] array, ref int index)
    {
        for (int i = 0; i < array.Length; i++)
        {
            inputTensor[0, new_sample_index, index] = array[i];
            index += 1;
        }
    }

    private void debugInputTensor()
    {
        StringBuilder myString = new StringBuilder();
        for (int i = 0; i < input_length; i++)
        {
            myString.Append(",");
            myString.Append(inputTensor[0, new_sample_index, i]);
        }
        Debug.Log(myString);
    }
}