﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TensorFlow;
using System.Text;

public class LeapNN : MonoBehaviour
{
    public TextAsset graphModel;
    private byte[] graphModelBytes;

    private const int input_length = 63;   //neural network input size  
    private const int output_length = 24;   //neural network output size 

    private float[,] inputTensor = new float[1, input_length];
    private float[] outputArray = new float[output_length];

    private float[] magnitudeArray = new float[(input_length - 6) / 3];
    //private int chosen_sign;
    private bool newPrediction = false;

    
    // Start is called before the first frame update
    void Start()
    {
        graphModelBytes = graphModel.bytes;
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */
    
    
    private float[] evaluate()
    {
        float[,] outputTensor;

        using (var graph = new TFGraph())
        {
            graph.Import(graphModelBytes);
            var session = new TFSession(graph);
            var runner = session.GetRunner();

            debugInputTensor();

            TFTensor input = inputTensor;

            runner.AddInput(graph["input"][0], input);

            runner.Fetch(graph["output"][0]); 

            outputTensor = runner.Run()[0].GetValue() as float[,];

            // frees up resources - very important if you are running graph > 400 or so times
            session.Dispose();
            graph.Dispose();
        }

        float[] outputArray = new float[outputTensor.GetLength(1)];

        for (int i = 0; i < outputTensor.GetLength(1); i++)
        {
            outputArray[i] = outputTensor[0, i];
        }

        return outputArray;
    }

    /*
    public float[] evaluateLeapData(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        fillInputTensor(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
        return evaluate();
    }
    */

    public void makePrediction()
    {
        outputArray = evaluate();
        newPrediction = true;
    }

    public float[] getPrediction()
    {
        newPrediction = false;
        return outputArray;
    }

    public bool predicitionAvailable()
    {
        return newPrediction;
    }

    public void updateLeapData(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        convertToVectors(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
        getMagnitudes();    //get magnitude for each vector in input array
        inputToNormalVectors(); //convert input vectors to normal vectors
    }

    



    private void convertToVectors(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        int inputIndex = 0;
        bool skipThumbBone = true;

        
        for (int i = 0; i < fingerTipMatrix.Length; i++)
        {
            for (int j = 0; j < fingerTipMatrix[i].Length; j++)
            {
                inputTensor[0, inputIndex] = fingerTipMatrix[i][j][0] - fingerBaseMatrix[i][j][0];      
                inputTensor[0, inputIndex + 1]  = fingerTipMatrix[i][j][1] - fingerBaseMatrix[i][j][1];
                inputTensor[0, inputIndex + 2]  = fingerTipMatrix[i][j][2] - fingerBaseMatrix[i][j][2];

                if (!(inputIndex == 9 && skipThumbBone)){
                    inputIndex += 3;
                }
                else
                {
                    skipThumbBone = false;
                }

                //inputIndex += 3;
                //Debug.Log(inputIndex);
            }        
        }
        for (int i = 0; i < 3; i++)
        {
            inputTensor[0, inputIndex] = handOrientation[i];
            inputIndex += 1;
        }
        
        for (int i = 0; i < 3; i++)
        {
            inputTensor[0, inputIndex] = armOrientation[i];
            inputIndex += 1;
        }
        
    }

    private void getMagnitudes()
    {
        for (int i = 0; i < (input_length - 6) / 3; i++)
        {
            magnitudeArray[i] = (float)Math.Sqrt(Math.Pow(inputTensor[0, i * 3], 2) + Math.Pow(inputTensor[0, i * 3 + 1], 2) + Math.Pow(inputTensor[0, i * 3 + 2], 2));
        }
    }

    private void inputToNormalVectors()
    {
        for (int i = 0; i < (input_length - 6) / 3; i++)
        {
            inputTensor[0, i * 3] = inputTensor[0, i * 3] / magnitudeArray[i];
            inputTensor[0, i * 3 + 1] = inputTensor[0, i * 3 + 1] / magnitudeArray[i];
            inputTensor[0, i * 3 + 2] = inputTensor[0, i * 3 + 2] / magnitudeArray[i];
        }

        inputTensor[0, 3 * 3] = 0.0F;
        inputTensor[0, 3 * 3 + 1] = 0.0F;
        inputTensor[0, 3 * 3 + 2] = 0.0F;

    }




    private void fillInputTensor(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation)
    {
        int index = 0;
        flattenToInput(ref fingerTipMatrix, ref index);
        //Debug.Log(index);
    }

    private void flattenToInput(ref float[][][] matrix, ref int index)
    {
        for (int i = 0; i < matrix.Length; i++)
        {
            flattenToInput(ref matrix[i], ref index);
        }
    }

    private void flattenToInput(ref float[][] matrix, ref int index)
    {
        for (int i = 0; i < matrix.Length; i++)
        {
            flattenToInput(ref matrix[i], ref index);
        }
    }

    private void flattenToInput(ref float[] array, ref int index)
    {
        for (int i = 0; i < array.Length; i++)
        {
            inputTensor[0,index] = array[i];
            index += 1;
        }
    }

    private void debugInputTensor()
    {
        StringBuilder myString = new StringBuilder();
        for (int i = 0; i < input_length; i++)
        {
            myString.Append(",");
            myString.Append(inputTensor[0, i]);
        }
        Debug.Log(myString);
    }
}
