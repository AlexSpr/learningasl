﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Leap;
//using Leap.Unity;

public class HandTracker: MonoBehaviour {

    private Controller leapController = new Controller();
    //Hand Data  
    private float[][][] boneBasesMatrix = new float[5][][]; // holds coordinates data of all finger bones basis 
    private float[][][] boneTipMatrix = new float[5][][]; // holds coordinates data of all finger bones tips
    private float[] handOrientation = new float[3];
    private float[] armOrientation = new float[3];

    public bool handTracked;
    public float leadHandConfindece;
    public bool leadHandIsRight;
    private int handCount;

    Matrix handTransform;
    Hand leadHand;

    void Start()
    {
        initiateBoneMatrixes();
    }

    void Update()
    {
        handTracked = trackHand();
    }

    public bool trackHand()
    {
        Frame frame = leapController.Frame(); // controller is a Controller object

        if (frame.Hands.Count > 0)
        {
            handCount = frame.Hands.Count;
            List<Hand> hands = frame.Hands;
            leadHand = hands[0]; //first detected Hand is used for tracking 
            collectHandData(ref leadHand);


            return true;
        }
        else
        {
            handCount = 0;
            resetBoneVectorMatrixes();
            return false;
        }
    }


    private void collectHandData(ref Hand hand)
    {
        leadHandConfindece = hand.Confidence;
        leadHandIsRight = hand.IsRight;

        Vector handXBasis = hand.PalmNormal.Cross(hand.Direction).Normalized;
        Vector handYBasis = -hand.PalmNormal;
        Vector handZBasis = -hand.Direction;

        Vector handOrigin = hand.PalmPosition;

        if (leadHandIsRight)
        {
            handTransform = new Matrix(handXBasis, handYBasis, handZBasis, handOrigin);
        }
        else
        {
            handTransform = new Matrix(-handXBasis, handYBasis, handZBasis, handOrigin);
        }

        handTransform = handTransform.RigidInverse();

        int fingerCounter = 0;
        List<Finger> fingers = hand.Fingers;
        foreach (Finger finger in fingers) //go through each finger of the first hand
        {
            int boneCounter = 3;
            foreach (Bone bone in finger.bones)  //go through each bone of finger
            {
                Vector boneBase = handTransform.TransformPoint(bone.PrevJoint);
                Vector boneTip = handTransform.TransformPoint(bone.NextJoint);

                float boneXbase = boneBase.x;
                float boneYbase = boneBase.y;
                float boneZbase = boneBase.z;

                float boneXtip = boneTip.x;
                float boneYtip = boneTip.y;
                float boneZtip = boneTip.z;

                boneBasesMatrix[fingerCounter][boneCounter][0] = boneXbase;
                boneBasesMatrix[fingerCounter][boneCounter][1] = boneYbase;
                boneBasesMatrix[fingerCounter][boneCounter][2] = boneZbase;

                boneTipMatrix[fingerCounter][boneCounter][0] = boneXtip;
                boneTipMatrix[fingerCounter][boneCounter][1] = boneYtip;
                boneTipMatrix[fingerCounter][boneCounter][2] = boneZtip;

                boneCounter -= 1;
            }
            fingerCounter += 1;
        }

        Arm arm = hand.Arm;
        Vector armDirection = arm.Direction;
        if (leadHandIsRight)
        {
            handOrientation[0] = hand.Direction.Pitch;
            handOrientation[1] = hand.Direction.Yaw;
            handOrientation[2] = hand.PalmNormal.Roll;

            armOrientation[0] = armDirection.x;
            armOrientation[1] = armDirection.y;
            armOrientation[2] = armDirection.z;
        }
        else
        {
            handOrientation[0] = hand.Direction.Pitch;
            handOrientation[1] = -hand.Direction.Yaw;
            handOrientation[2] = -hand.PalmNormal.Roll;

            armOrientation[0] = -armDirection.x;
            armOrientation[1] = armDirection.y;
            armOrientation[2] = armDirection.z;
        }

    }
    
    public int numberOfTrackedHands()
    {
        return handCount;
    }

    public float[][][] getFingerBaseMatrix()
    {
        return boneBasesMatrix;
    }

    public float[][][] getFingerTipMatrix()
    {
        return boneTipMatrix;
    }

    public float[] getHandOrientation()
    {
        return handOrientation;
    }

    public float[] getArmOrientation()
    {
        return armOrientation;
    }
 
    private void initiateBoneMatrixes()
    {
        for (int fingerIndex = 0; fingerIndex < boneBasesMatrix.Length; fingerIndex++)
        {
            boneBasesMatrix[fingerIndex] = new float[4][]; 
            for (int boneIndex = 0; boneIndex < boneBasesMatrix[fingerIndex].Length; boneIndex++)
            {
                boneBasesMatrix[fingerIndex][boneIndex] = new float[3];
            }

        }

        for (int fingerIndex = 0; fingerIndex < boneTipMatrix.Length; fingerIndex++)
        {
            boneTipMatrix[fingerIndex] = new float[4][];
            for (int boneIndex = 0; boneIndex < boneTipMatrix[fingerIndex].Length; boneIndex++)
            {
                boneTipMatrix[fingerIndex][boneIndex] = new float[3];
            }

        }
    }

    private void resetBoneVectorMatrixes()
    {
        for (int fingerIndex = 0; fingerIndex < boneBasesMatrix.Length; fingerIndex++)
        {
            for (int boneIndex = 0; boneIndex < boneBasesMatrix[fingerIndex].Length; boneIndex++)
            {
                Array.Clear(boneBasesMatrix[fingerIndex][boneIndex], 0, boneBasesMatrix[fingerIndex][boneIndex].Length);
            }
        }

        for (int fingerIndex = 0; fingerIndex < boneTipMatrix.Length; fingerIndex++)
        {
            for (int boneIndex = 0; boneIndex < boneTipMatrix[fingerIndex].Length; boneIndex++)
            {
                Array.Clear(boneTipMatrix[fingerIndex][boneIndex], 0, boneTipMatrix[fingerIndex][boneIndex].Length);
            }
        }
    }
}
