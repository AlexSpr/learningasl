﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMarker : MonoBehaviour
{
    public Color outlineColor;
    public float outlineWidth;

    private GameObject capsuleHandModels;
    private GameObject riggedHandModels;

    private GameObject rightHandMesh;
    private GameObject leftHandMesh;

    private Material[] outlines = new Material[2];

    private bool currentLeadHandIsRight;
    private bool LeadHandIsRight;
    private bool markLeadHand;

    SceneController sceneController;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.FindObjectOfType<SceneController>();

        capsuleHandModels = GameObject.Find("Capsule Hand Models");
        riggedHandModels = GameObject.Find("Rigged Hand Models");
        //Debug.Log(riggedHandModels);
        leftHandMesh = riggedHandModels.transform.Find("LoPoly Rigged Hand Left/LoPoly_Hand_Mesh_Left").gameObject;
        outlines[0] = leftHandMesh.GetComponent<Renderer>().materials[1];

        rightHandMesh = riggedHandModels.transform.Find("LoPoly Rigged Hand Right/LoPoly_Hand_Mesh_Right").gameObject;
        outlines[1] = rightHandMesh.GetComponent<Renderer>().materials[1];// mats[1];



        outlines[0].SetColor("_OutlineColor", outlineColor);
        outlines[1].SetColor("_OutlineColor", outlineColor);

        outlines[0].SetFloat("_Outline", 0);
        outlines[1].SetFloat("_Outline", 0);

    }

    // Update is called once per frame
    void Update()
    {
   
        currentLeadHandIsRight = sceneController.getleadHandIsRight();
        markLeadHand = sceneController.getMarkLeadHand();

        if (markLeadHand) { 
            if (currentLeadHandIsRight)
            {
                outlines[0].SetFloat("_Outline", 0);
                outlines[1].SetFloat("_Outline", outlineWidth);
            }
            else
            {
                outlines[0].SetFloat("_Outline", outlineWidth);
                outlines[1].SetFloat("_Outline", 0);
            }
        }
        else
        {
            outlines[0].SetFloat("_Outline", 0);
            outlines[1].SetFloat("_Outline", 0);
        }
    }
}
