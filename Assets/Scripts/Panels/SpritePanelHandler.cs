﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SpritePanelHandler : MonoBehaviour
{
    // Use this for initialization
    private GameObject VideoSignPlayerObj;
    private VideoPlayer videoSignPlayer;
    private RawImage videoSignImage;
    private RawImage signImage;
    private Color imgColor;

    private string spritePath = "Sprites";
    private string videoSignPath = "videoSigns";

    private SignInfo signInfo = new SignInfo();
    private string[] signSpriteNames; //= new string[36];
    private string[] videoSignsNames; //= new string[36];
    private bool imageSetForFirstTime = true;
    private bool leadHandIsRight = false;
    private bool currentLeadHandIsRight;
    private bool videoSignsOn = false;

    private int videoSignIndex = int.MaxValue;
    private int currentSignIndex = int.MaxValue; //set to large number to avoid compare issues with sign index on first init
    private int lastSignIndex = 0;

    SceneController sceneController;
    //RecSignsController recController;

    void Start()
    {
        signSpriteNames = new string[signInfo.Signs.Length];
        videoSignsNames = new string[signInfo.Signs.Length];


        signImage = transform.Find("CurrentSpriteDisplay").GetComponent<RawImage>();
        videoSignPlayer = transform.Find("VideoSignPlayer").GetComponent<VideoPlayer>();
        videoSignImage = transform.Find("VideoSignPlayer").GetComponent<RawImage>();
        VideoSignPlayerObj = transform.Find("VideoSignPlayer").gameObject;
        //VideoSignPlayerObj.SetActive(false);
        imgColor = new Color(0f, 0f, 0f, 0f); //have image faded 

        signImage.color = imgColor;
        videoSignImage.color = imgColor;

        for (int i = 0; i < videoSignsNames.Length; i++)
        {
            videoSignsNames[i] = "0_0";
        }

        //getSignSpritesFromResources();
        //getVideoSignsFromResources();
        getSignsFromResources(ref signSpriteNames, typeof(Texture2D));
        getSignsFromResources(ref videoSignsNames, typeof(VideoClip));


        Debug.Log(signSpriteNames[0]);
        Debug.Log(videoSignsNames[0]);

        sceneController = GameObject.FindObjectOfType<SceneController>();
    }

    // Update is called once per frame
    void Update()
    {
        lastSignIndex = currentSignIndex;


        if (sceneController != null)
        {
            currentSignIndex = sceneController.getCurrentSignIndex();
            currentLeadHandIsRight = sceneController.getleadHandIsRight();
            videoSignsOn = sceneController.videoSignsOn();
        } 

        if (videoSignsOn)
        {
            VideoSignPlayerObj.SetActive(true);
        }
        else
        {
            VideoSignPlayerObj.SetActive(false);
        }

        if (currentSignIndex != lastSignIndex)
        {
            if (imageSetForFirstTime)
            {
                setVideoToOpaqueOnFirst();
                imageSetForFirstTime = setImageToOpaqueOnFirst();
            }

            signImage.texture = loadPNG();
            //videoSignPlayer.source = 
            videoSignPlayer.clip = loadVideo();
        }

        if (!imageSetForFirstTime)
        {
            if (currentLeadHandIsRight != leadHandIsRight)
            {
                flipSprite();
            }

            leadHandIsRight = currentLeadHandIsRight;
        }
    }

    private void flipSprite() //flips video automatically as well
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private bool setImageToOpaqueOnFirst()
    {
        imgColor = new Color(1f, 1f, 1f, 1f);
        signImage.color = imgColor;
        return false;
    }

    private bool setVideoToOpaqueOnFirst()
    {
        imgColor = new Color(1f, 1f, 1f, 1f);
        videoSignImage.color = imgColor;
        return false;
    }

    private void getSignsFromResources(ref string[] signArray, System.Type type)
    {
        UnityEngine.Object[] signObjects;
        signObjects = Resources.LoadAll(spritePath, type);
        foreach (var sObject in signObjects)
        {
            string signObjectName = sObject.name;
            string signName = signObjectName.Split('_')[1];
            int signIndex = Array.IndexOf(signInfo.Signs, signName);
            //Debug.Log(signIndex);
            //int signIndex = int.Parse(spriteSignName.Split('_').First());
            if (signIndex >= 0) {
                signArray[signIndex] = signObjectName;
            }
        }
    }
    /*
    private void getSignSpritesFromResources()
    {
        UnityEngine.Object[] signSprites;
        signSprites = Resources.LoadAll(spritePath, typeof(Texture2D));
        foreach (var sprite in signSprites)
        {
            string spriteSignName = sprite.name;
            int signIndex = int.Parse(spriteSignName.Split('_').First());
            signSpriteNames[signIndex] = spriteSignName;
        }
    }

    private void getVideoSignsFromResources()
    {
        UnityEngine.Object[] videoSigns;
        videoSigns = Resources.LoadAll(videoSignPath, typeof(VideoClip));
        foreach (var video in videoSigns)
        {
            string videoName = video.name;
            int signIndex = int.Parse(videoName.Split('_').First());
            videoSignsNames[signIndex] = videoName;
        }
    }
    */



    private Texture2D loadPNG()
    {
        Texture2D tex = null;
        tex = new Texture2D(2, 2);
        tex = Resources.Load<Texture2D>("Sprites/" + signSpriteNames[currentSignIndex]);

        return tex;
    }

    private VideoClip loadVideo()
    {
        return Resources.Load<VideoClip>("videoSigns/" + signSpriteNames[currentSignIndex]);
    }
}
