﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TogglePrefabHandler : MonoBehaviour
{
    [SerializeField] private Text toggleLabel;      //value of setting text
    [SerializeField] private Toggle toggle;         

    private int settingsIndex;         //index of setting in Super UserSetting class. Used when saving new setting value

    public delegate void toggleDelegate(int settingIndex, bool isOn);   //delegate holding function from SettingsHandler to be executed when toggle is pressed 
    private toggleDelegate m_onToggleChange;

    public void setToggleSetting(int index, bool isOn, string label, toggleDelegate onToggleChange)
    {
        settingsIndex = index;                   //sets setting index of togglePrefab
        toggle.isOn = isOn;                      //sets toggle to true or false
        toggleLabel.text = label;                //sets setting name of togglepPefab
        m_onToggleChange = onToggleChange;      //sets onToggleChange function from SettingsHandler as delegate 
    }

    void Start()
    {
        toggle.onValueChanged.AddListener(delegate { m_onToggleChange(settingsIndex, toggle.isOn); }); //SettingsHandler function call to update toggle value
    }

}
