﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncrementPrefabHandler: MonoBehaviour
{
    [SerializeField] private Text incrementSettingLabel;    //name of setting text
    [SerializeField] private Text incrementValueLabel;      //value of setting text
    [SerializeField] private Button increment;              //button that increments by 1 
    [SerializeField] private Button incrementX2;            //button that increments by 2 
    [SerializeField] private Button decrement;              //button that decrements by 1 
    [SerializeField] private Button decrementX2;            //button that decrements by 2 

    private int settingsIndex = 0; //index of setting in Super UserSetting class. Used when saving new setting value
    private float incrementValue = 3;   //value of setting

    public delegate void incrementDelegate(int settingIndex, float settingValue);   //delegate holding function from SettingsHandler to be executed when button is pressed 
    public incrementDelegate m_onIncrementValueChange; //delegate variable 

    //incrementSettingprefab initialization function that is called by SettingsHandler class when after prefab is instantiated
    public void setIncrementSetting(int index, float value, string label, incrementDelegate onIncrementValueChange)   
    {
        settingsIndex = index;                                  //sets setting index of incrementprefab
        incrementValue = value;                                 //sets setting value of incrementprefab
        updateIncrementValueLabel();                            //sets setting value text to value
        incrementSettingLabel.text = label;                     //sets setting name of incrementprefab
        m_onIncrementValueChange = onIncrementValueChange;      //sets onIncrementValueChange function from SettingsHandler as delegate 
    }

    //Prefab initialization. Sets Increment values for each button with delegates 
    void Start () { 
        increment.onClick.AddListener(delegate { changeIncrementValue(1); });
        incrementX2.onClick.AddListener(delegate { changeIncrementValue(2); });
        decrement.onClick.AddListener(delegate { changeIncrementValue(-1); });
        decrementX2.onClick.AddListener(delegate { changeIncrementValue(-2); });
    }

    private void changeIncrementValue(float value) //called on button press
    {
        if (incrementValue + value < 1)  //when new value is smaller t
        {
            incrementValue = 1;
        }
        else                            //otherwise new value is value
        {
            incrementValue += value;
        }
        updateIncrementValueLabel(); //updates setting value text to value
        m_onIncrementValueChange(settingsIndex, incrementValue);   //SettingsHandler function call to update setting value
    }

    private void updateIncrementValueLabel()   //changes value text to value
    {
        incrementValueLabel.text = (incrementValue).ToString();
    }
}
