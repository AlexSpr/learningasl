﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashCanvasHandler : MonoBehaviour {

    private CanvasGroup flashCanvas;
    private Text currentTimeText;
    bool dataJustRecorded = false;
    // Use this for initialization
    void Start () {
        flashCanvas = GetComponent<CanvasGroup>();
        currentTimeText = GameObject.Find("flashText").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (dataJustRecorded)
        {
            fadeFlash();
        }
    }

    public void createFlash(string currentTimeAsString)
    {
        currentTimeText.text = currentTimeAsString;
        dataJustRecorded = true;
        flashCanvas.alpha = 1;
    }
    

    private void fadeFlash()
    {
        flashCanvas.alpha -= Time.deltaTime;
        if (flashCanvas.alpha <= 0)
        {
            flashCanvas.alpha = 0;
            dataJustRecorded = false;
        }
    }
}
