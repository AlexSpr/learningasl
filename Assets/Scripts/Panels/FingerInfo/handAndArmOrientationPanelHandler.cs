﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class handAndArmOrientationPanelHandler : MonoBehaviour {

    public GameObject handPitchValue;
    public GameObject handYawValue;
    public GameObject handRollValue;

    public GameObject armX;
    public GameObject armY;
    public GameObject armZ;

    HandTracker tracker;
    // Use this for initialization
    void Start () {
        tracker = GameObject.FindObjectOfType<HandTracker>();
    }
	
	// Update is called once per frame
	void Update () {
        if (tracker.handTracked)
        {
            float[] handOrientation = tracker.getHandOrientation();
            handPitchValue.GetComponent<Text>().text = Math.Round(handOrientation[0], 2).ToString();
            handYawValue.GetComponent<Text>().text = Math.Round(handOrientation[1], 2).ToString();
            handRollValue.GetComponent<Text>().text = Math.Round(handOrientation[2], 2).ToString();

            float[] armOrientation = tracker.getArmOrientation();
            armX.GetComponent<Text>().text = Math.Round(armOrientation[0]).ToString();
            armY.GetComponent<Text>().text = Math.Round(armOrientation[1]).ToString();
            armZ.GetComponent<Text>().text = Math.Round(armOrientation[2]).ToString();
        }
    }
}
