﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class FingerAnglePanelHandler : MonoBehaviour {

    public GameObject fingerCoordinatesPrefab;

    HandTracker tracker;

    DataAnalyzer analyzer;

    private string[] fingerNames = new string[] { "Thumb", "Index", "Middle", "Ring", "Pinky" };

    // Use this for initialization
    void Start()
    {
        analyzer = GameObject.FindObjectOfType<DataAnalyzer>();
        tracker = GameObject.FindObjectOfType<HandTracker>();
        for (int i = 0; i < 5; i++)
        {
            GameObject go = (GameObject)Instantiate(fingerCoordinatesPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("fingerName").GetComponent<Text>().text = fingerNames[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tracker.handTracked)
        {
            float[][] fingerAngles = analyzer.getfingerAngles();

            int fingerIndex = 0;

            foreach (Transform child in transform)
            {
                child.transform.Find("distalBone").GetComponent<Text>().text = getFingerAngleString(ref fingerAngles, ref fingerIndex, 0);
                child.transform.Find("intermediateBone").GetComponent<Text>().text = getFingerAngleString(ref fingerAngles, ref fingerIndex, 1);
                child.transform.Find("proximalBone").GetComponent<Text>().text = getFingerAngleString(ref fingerAngles, ref fingerIndex, 2);
                child.transform.Find("metacarpalBone").GetComponent<Text>().text = getFingerAngleString(ref fingerAngles, ref fingerIndex, 3);
                fingerIndex += 1;
            }
        }
    }

    private string getFingerAngleString(ref float[][] fingerAngleMatrix, ref int fingerIndex, int angleIndex)
    {
        string text = Math.Round(fingerAngleMatrix[fingerIndex][angleIndex], 2).ToString();//fingerAngleMatrix[fingerIndex][angleIndex].ToString();
        return text;
    }
}
