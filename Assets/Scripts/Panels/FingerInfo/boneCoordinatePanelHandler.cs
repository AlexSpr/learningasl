﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class boneCoordinatePanelHandler : MonoBehaviour {

    public GameObject fingerCoordinatesPrefab;

    HandTracker tracker;

    DataAnalyzer analyzer;

    bool alreadyWritten = false;

    private string[] fingerNames = new string[] { "Thumb", "Index", "Middle", "Ring", "Pinky" };

    // Use this for initialization
    void Start () {
        tracker = GameObject.FindObjectOfType<HandTracker>();
        analyzer = GameObject.FindObjectOfType<DataAnalyzer>();
        for (int i = 0; i < 5; i++)
        {
            GameObject go = (GameObject)Instantiate(fingerCoordinatesPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("fingerName").GetComponent<Text>().text = fingerNames[i];
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (tracker.handTracked)
        {
            float[][][] fingerBoneVectors = analyzer.getFingerVectorMatrix();
            int fingerIndex = 0;
            
            foreach (Transform child in transform)
            {
                child.transform.Find("distalBone").GetComponent<Text>().text = getBoneCoordinateString(ref fingerBoneVectors, ref fingerIndex, 0);
                child.transform.Find("intermediateBone").GetComponent<Text>().text = getBoneCoordinateString(ref fingerBoneVectors, ref fingerIndex, 1);
                child.transform.Find("proximalBone").GetComponent<Text>().text = getBoneCoordinateString(ref fingerBoneVectors, ref fingerIndex, 2);
                child.transform.Find("metacarpalBone").GetComponent<Text>().text = getBoneCoordinateString(ref fingerBoneVectors, ref fingerIndex, 3);
                fingerIndex += 1;
            }
            if (alreadyWritten)
                alreadyWritten = false;
        }
        else if (!tracker.handTracked && !alreadyWritten)
        {
            foreach (Transform child in transform)
            {
                child.transform.Find("distalBone").GetComponent<Text>().text = "no data";
                child.transform.Find("intermediateBone").GetComponent<Text>().text = "no data";
                child.transform.Find("proximalBone").GetComponent<Text>().text = "no data";
                child.transform.Find("metacarpalBone").GetComponent<Text>().text = "no data";
            }
            alreadyWritten = true;
        }
	}

    private string getBoneCoordinateString(ref float[][][] fingerBoneVectors, ref int fingerIndex, int boneIndex)
    { 
        string text = "(" + Math.Round(fingerBoneVectors[fingerIndex][boneIndex][0]).ToString() + ", " + Math.Round(fingerBoneVectors[fingerIndex][boneIndex][1]).ToString() + ", " + Math.Round(fingerBoneVectors[fingerIndex][boneIndex][2]).ToString() + ")";
        return text;
    }
}
