﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Leap;
using Leap.Unity;

public class cameraOutputHandler : MonoBehaviour {

    public RawImage cameraOutput;
    private Color imgColor;
    ImageRetriever imageRetriever;

    Texture2D outputTexture = null;

    private bool imageSetForFirstTime = true;
    // Use this for initialization
    void Start () {
        imgColor = new Color(0f, 0f, 0f, 0f); //have image faded 
        cameraOutput.color = imgColor;
        imageRetriever = GameObject.FindObjectOfType<ImageRetriever>();
    }
	
	// Update is called once per frame
	void Update() {

        outputTexture = null;

        outputTexture = imageRetriever.getCurrentImageTexture();

        if (outputTexture != null)
        {
            if (imageSetForFirstTime)
            {
                imageSetForFirstTime = setImageToOpaqueOnFirst();
            }
            cameraOutput.texture = outputTexture;
        }
    }

    bool setImageToOpaqueOnFirst()
    {
        imgColor = new Color(1f, 1f, 1f, 1f);
        cameraOutput.color = imgColor;
        return false;
    }

    Texture2D loadImageTexture(byte[] fileData)
    {
        Texture2D tex = null;
  
        tex = new Texture2D(2, 2);
        tex.LoadRawTextureData(fileData); //..this will auto-resize the texture dimensions.
        tex.Apply();
        return tex;
    }

}
