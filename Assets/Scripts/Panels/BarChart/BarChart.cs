﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BarChart : MonoBehaviour
{
    //public GameObject barChartPrefab;
    public Bar barPrefab;
    // Start is called before the first frame update
    List<Bar> bars = new List<Bar>();
    private SignInfo signInfo = new SignInfo();

    Color[] barColors = new Color[] { Color.blue, Color.cyan, Color.green, Color.yellow, Color.red };
    float chartWidth;

    private float changeValue = 0.025f;

    int numberOfBars = 5;

    private bool barsNeedChange = false;

    private float[] lastPredictionsPercentage;
    private int[] lastPredictionsIndex;
    private string[] lastPredictedSigns;

    private float[] barValues;

    void Start()
    {
        lastPredictionsPercentage = new float[numberOfBars];
        lastPredictionsIndex = new int[numberOfBars];
        lastPredictedSigns = new string[numberOfBars];

        barValues = new float[numberOfBars];

        for (int i = 0; i < numberOfBars; i++)
        {
            barValues[i] = 0;
        }

        chartWidth = Screen.width + GetComponent<RectTransform>().sizeDelta.x;
        setupBarChart();
    }

    // Update is called once per frame
    void Update()
    {
        if (barsNeedChange)
            changeBars();
    }

    public void changeBars()
    {
        int finished_count = 0;
        for (int i = 0; i < numberOfBars; i++)
        {
            if (barValues[i] < lastPredictionsPercentage[i])
            {
                barValues[i] += changeValue;
            }
            else if (barValues[i] > lastPredictionsPercentage[i])
            {
                barValues[i] -= changeValue;
            }

            if (Math.Abs(lastPredictionsPercentage[i] - barValues[i]) < changeValue)
            {
                barValues[i] = lastPredictionsPercentage[i];
                finished_count += 1;
            }

            int[] rgbValues = signInfo.signColors[lastPredictionsIndex[i]];
            Color signColor = new Color(rgbValues[0], rgbValues[1], rgbValues[2]);
            bars[i].updateBar(lastPredictedSigns[i], barValues[i], signColor);
        }
        if (finished_count == numberOfBars)
        {
            barsNeedChange = false;
        }
    }

    public void updateBarChart(float[] predictions)
    {

        for (int i = 0; i < numberOfBars; i++)
        {
            float maxValue = predictions.Max();
            int predictedSignIndex = predictions.ToList().IndexOf(maxValue);
            string predictedSign = signInfo.Signs[predictedSignIndex];

            lastPredictionsPercentage[i] = predictions[predictedSignIndex];
            lastPredictionsIndex[i] = predictedSignIndex;
            lastPredictedSigns[i] = predictedSign;            /*
            string predictedSign = signInfo.Signs[predictedSignIndex];
            if (predictions[predictedSignIndex] > 0)
            {
                bars[i].updateBar(predictedSign, predictions[predictedSignIndex]);
            }
            */
            predictions[predictedSignIndex] = 0;
        }
        barsNeedChange = true;
    }

    /*
    public void updateBarChart(float[] predictions)
    {
        
        for (int i = 0; i < numberOfBars; i++)
        {
            float maxValue = predictions.Max();
            int predictedSignIndex = predictions.ToList().IndexOf(maxValue);
            string predictedSign = signInfo.Signs[predictedSignIndex];
            if (predictions[predictedSignIndex] > 0)
            {
                bars[i].updateBar(predictedSign, predictions[predictedSignIndex]);
            }
            
            predictions[predictedSignIndex] = 0;
        }
    }
    */
    private void setupBarChart()
    {
        for (int i = 0; i < numberOfBars; i++)
        {
            Bar newBar = (Bar)Instantiate(barPrefab);
            newBar.transform.SetParent(transform);
            newBar.setFullBarSize();
            newBar.bar.GetComponent<Image>().color = barColors[i];

            newBar.updateBar("No Prediction", 0f);
            bars.Add(newBar);
        }
    }

}
