﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Text;
using System;

public class Bar : MonoBehaviour
{
    public Image bar;
    public Text predictedSign;
    public Text confidenceValue;

    private float originalBarSize;

    public void setFullBarSize()
    {
        RectTransform rt = bar.GetComponent<RectTransform>();
        originalBarSize = rt.sizeDelta.x;
    }

    public void updateBar(string Sign, float confidence)
    {
        RectTransform rt = bar.GetComponent<RectTransform>();

        rt.sizeDelta = new Vector2(originalBarSize * confidence, rt.sizeDelta.y);

        StringBuilder confidenceText = new StringBuilder();
        confidenceText.Append(Math.Round(confidence * 100, 2));
        confidenceText.Append("%");

        confidenceValue.color = Color.white;
        confidenceValue.GetComponent<Text>().text = confidenceText.ToString();
        predictedSign.color = Color.white;
        predictedSign.GetComponent<Text>().text = Sign;
    }

    public void updateBar(string Sign, float confidence, Color color)
    {
        RectTransform rt = bar.GetComponent<RectTransform>();

        rt.sizeDelta = new Vector2(originalBarSize * confidence, rt.sizeDelta.y);

        StringBuilder confidenceText = new StringBuilder();
        confidenceText.Append(Math.Round(confidence * 100, 2));
        confidenceText.Append("%");

        confidenceValue.color = Color.white;
        confidenceValue.GetComponent<Text>().text = confidenceText.ToString();
        predictedSign.color = Color.white;
        predictedSign.GetComponent<Text>().text = Sign;

        bar.GetComponent<Image>().color = color;
    }
}
