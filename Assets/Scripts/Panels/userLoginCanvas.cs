﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class userLoginCanvas : MonoBehaviour
{
    // Start is called before the first frame update
    private string userName = "Unknown";

    void Start()
    {
        var input = GetComponentInChildren<InputField>();
        var se = new InputField.SubmitEvent();
        se.AddListener(setName);
        input.onEndEdit = se;
    }

    private void setName(string arg0)
    {
        userName = arg0;
    }

    public string getName()
    {
        return userName;
    }
}
