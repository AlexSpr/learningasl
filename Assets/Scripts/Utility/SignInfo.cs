﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignInfo {

    private static string[] allSigns = new string[] {  "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
    "S", "T", "U", "V", "W", "X", "Y", "Z"
    };

    private static string[] alphabetSigns = new string[] {  "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                                                    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                                                    "U", "V", "W", "X", "Y", "Z" };


    private static float[] alphabetSignsMinConfidenceValues = new float[] { 0.94f, 0.99f, 0.96f, 0.95f, 0.98f, 0.99f, 0.91f, 0.96f, 0.99f, 0.91f,
                                                                            0.87f, 0.99f, 0.67f, 0.57f, 0.97f, 0.87f, 0.79f, 0.71f, 0.61f, 0.78f,
                                                                            0.85f, 0.94f, 0.98f, 0.91f, 0.97f, 0.90f};


    private static string[] noMotionAlphabetSigns = new string[] {  "A", "B", "C", "D", "E", "F", "G", "H", "I",
                                                            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                                                            "U", "V", "W", "X", "Y" };

    private static string[] onlyMotionAlphabetSigns = new string[] { "J", "Z" };

    public string[] Signs; // = noMotionAlphabetSigns;
    public float[] SignsMinConfidenceValues; // = noMotionAlphabetSigns;

    public SignInfo()
    {
        Signs = alphabetSigns; // onlyMotionAlphabetSigns; //noMotionAlphabetSigns;
        SignsMinConfidenceValues = alphabetSignsMinConfidenceValues;
    }

    public int[][] signColors =
    {
        new int[] {255,0,0},
        new int[] {0,255,0},
        new int[] {0,0,255},
        new int[] {255,255,0},
        new int[] {0,255,255},
        new int[] {255,0,255},
        new int[] {192,192,192},
        new int[] {128,0,0},
        new int[] {128,128,0},
        new int[] {0,128,0},
        new int[] {128,0,128},
        new int[] {0,128,128},
        new int[] {0,0,128},
        new int[] {220,20,60},
        new int[] {255,127,80},
        new int[] {255,140,0},
        new int[] {255,20,147},
        new int[] {255,250,205},
        new int[] {210,105,30},
        new int[] {112,128,144},
        new int[] {0,100,0},
        new int[] {46,139,87},
        new int[] {152,251,152},
        new int[] {205,92,92},
        new int[] {139,69,19},
        new int[] {244,164,96},
        new int[] { 245, 245, 245 }
    };

}
