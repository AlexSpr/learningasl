﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DataAnalyzer : MonoBehaviour {

    HandTracker tracker;
    private float[] vector = new float[3];

    private float[][][] fingerVectorMatrix = new float[5][][];
    //private float[][][] crossProductMatrix = new float[5][][];
    private float[][] fingerAngles = new float[5][];

    void Start()//public DataAnalyzer()
    {
        tracker = GameObject.FindObjectOfType<HandTracker>();
        initializeFingerMatrix();
        initializeFingerAngles();
        //initalizeDotProductMatrix();
        //initalizeCrossProductMatrix();
    }

    void Update()
    {
        if (tracker.handTracked)
        {
            float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
            float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
            calculateFingerVectorMatrix(ref fingerBaseMatrix, ref fingerTipMatrix);
            calculateFingerAngles(ref fingerVectorMatrix);
        }
    }

    public float[][] getfingerAngles()
    {
        return fingerAngles;
    }

    public float[][][] getFingerVectorMatrix()
    {
        return fingerVectorMatrix;
    }

    private float[][][] calculateFingerVectorMatrix(ref float[][][] matrixA, ref float[][][] matrixB)
    {
        for (int i = 0; i < matrixA.Length; i++)
        {
            for (int j = 0; j < matrixA[i].Length; j++)
            {
                vector = calculateVector(ref matrixA[i][j], ref matrixB[i][j]);
                fingerVectorMatrix[i][j][0] = vector[0];
                fingerVectorMatrix[i][j][1] = vector[1];
                fingerVectorMatrix[i][j][2] = vector[2];
            }
        }

        return fingerVectorMatrix;
    }

    private float[][] calculateFingerAngles(ref float[][][] fingerDirectiosnMatrix)
    {
        float angle = 0;
        for (int i = 0; i < fingerDirectiosnMatrix.Length; i++)
        {
            for (int j = 0; j < fingerDirectiosnMatrix[i].Length - 1; j++)
            {
                angle = (dotProduct(ref fingerDirectiosnMatrix[i][j], ref fingerDirectiosnMatrix[i][j + 1]) / (vectorMagnitude(ref fingerDirectiosnMatrix[i][j]) * vectorMagnitude(ref fingerDirectiosnMatrix[i][j + 1])));

                angle = 180 - toDegree((float)Math.Acos(angle));
                


                //angle = toDegree((float)Math.Acos(angle));


                    //Debug.Log(angleAdd);
                
                

                fingerAngles[i][j] = angle;
            }

            int proximalFingerIndex = fingerDirectiosnMatrix[i].Length - 2;
            /*
            float[] vectorA = new float[2];
            float[] vectorB = new float[] { 0, 1 };

            float yVec = fingerDirectiosnMatrix[i][proximalFingerIndex][1];
            float zVec = fingerDirectiosnMatrix[i][proximalFingerIndex][2];
            float[] newVector = new float[2];
            newVector[0] = yVec;
            newVector[1] = zVec;

            vectorA[0] = fingerDirectiosnMatrix[i][proximalFingerIndex][0];
            vectorA[1] = vectorMagnitude(ref newVector);//fingerDirectiosnMatrix[i][proximalFingerIndex][2];
            */
            float[] vectorA = new float[3];
            float[] vectorB = new float[] { 0, 0, 0 };

            vectorA = fingerDirectiosnMatrix[i][proximalFingerIndex];
            vectorB[1] = fingerDirectiosnMatrix[i][proximalFingerIndex][1];
            vectorB[2] = fingerDirectiosnMatrix[i][proximalFingerIndex][2];

            angle = (dotProduct(ref vectorA, ref vectorB) / (vectorMagnitude(ref vectorA) * vectorMagnitude(ref vectorB)));
            fingerAngles[i][fingerAngles[i].Length - 1] = toDegree((float)Math.Acos(Math.Abs(angle)));
        }

        return fingerAngles;
    }


    private float[] calculateVector(ref float[] coordinateA, ref float[] coordinateB)
    {
        for (int i = 0; i < vector.Length; i++)
        {
            vector[i] = coordinateB[i] - coordinateA[i];
        }
        return vector;
    } 
    /*
    private float[][][] calculateFingerCrossProducts(ref float[][][] fingerDirections)
    {
        for (int i = 0; i < fingerDirections.Length; i++)
        {
            for (int j = 0; j < fingerDirections[i].Length - 1; j++)
            {
                crossProductMatrix[i][j] = crossProduct(ref fingerDirections[i][j], ref fingerDirections[i][j + 1]);
            }
        }

        return crossProductMatrix;
    }
    */
    private void initializeFingerMatrix()
    {
        for (int fingerIndex = 0; fingerIndex < fingerVectorMatrix.Length; fingerIndex++)
        {
            fingerVectorMatrix[fingerIndex] = new float[4][];
            for (int boneIndex = 0; boneIndex < fingerVectorMatrix[fingerIndex].Length; boneIndex++)
            {
                fingerVectorMatrix[fingerIndex][boneIndex] = new float[3];
            }
        }
    }

    private void initializeFingerAngles()
    {
        for (int fingerIndex = 0; fingerIndex < fingerAngles.Length; fingerIndex++)
        {
            fingerAngles[fingerIndex] = new float[4];
        }
    }
    /*
    private void initalizeDotProductMatrix()
    {
        for (int fingerIndex = 0; fingerIndex < dotProductMatrix.Length; fingerIndex++)
        {
            dotProductMatrix[fingerIndex] = new float[4];
        }
    }

    private void initalizeCrossProductMatrix()
    {
        for (int fingerIndex = 0; fingerIndex < crossProductMatrix.Length; fingerIndex++)
        {
            crossProductMatrix[fingerIndex] = new float[4][];
            for (int boneIndex = 0; boneIndex < crossProductMatrix[fingerIndex].Length; boneIndex++)
            {
                crossProductMatrix[fingerIndex][boneIndex] = new float[3];
            }
        }
    }
    */
    private float dotProduct(ref float[] vectorA, ref float[] vectorB)
    {
        float dotP = 0;

        for(int i = 0; i < vectorA.Length; i++)
        {
            dotP += vectorA[i] * vectorB[i];
        }

        return dotP;
    }

    private float[] crossProduct(ref float[] vectorA, ref float[] vectorB)
    {
        float[] normalVector = new float[vectorA.Length];

        rotateArrayLeft(ref vectorA);
        rotateArrayLeft(ref vectorB);

        for (int i = 0; i < vectorA.Length; i++)
        {
            normalVector[i] = vectorA[0] * vectorB[1] - vectorA[1] * vectorB[0];

            rotateArrayLeft(ref vectorA);
            rotateArrayLeft(ref vectorB);
        }

        return normalVector;
    }

    private void rotateArrayLeft(ref float[] array)
    {
        float temp = array[0];
        for (int i = 0; i < array.Length - 1; i++)
        {
            array[i] = array[i + 1];
        }
        array[array.Length - 1] = temp;
    }

    private float vectorMagnitude(ref float[] vectorA)
    {
        float magnitude = 0 ;
        for (int i = 0; i < vectorA.Length; i++)
        {
            magnitude += (float)Math.Pow(vectorA[i], 2);
        }
        magnitude = (float)Math.Sqrt(magnitude);
        return magnitude;
    }

    private float toDegree(float radians)
    {
        return (float)(radians * 180 / Math.PI);
    }

}
