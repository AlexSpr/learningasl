﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class RecUserSettings: UserSettings
{
    //##//  INCREMENT SETTINGS  //##//
    public float countDownTimer;        // index 0
    public float samplesBeforeRotation;   // index 1

    public float numPredictionsPerSecond;
    //##//

    //^^ TOGGLE SETTINGS VARS//^^ 
    public bool signRotationIsOn;   // index 0 
    public bool showCamera;         // index 1
    public bool detailedHands;      // index 2
    public bool detailSigns;        // index 3
    public bool automaticPrediction;// index 4
    public bool videoSigns;         // index 5
    public bool markLeadHand;       // index 6
                                    //^^ 

    public float[] SignsMinConfidenceValues = new float[26];

    private static string[] alphabetSigns = new string[] {  "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                                                    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                                                    "U", "V", "W", "X", "Y", "Z" };
    //>><< TOGGLE SETTINGS FUNCTIONS//>><<
    public override bool[] getToggleValues() { return new bool[] { signRotationIsOn, showCamera, detailedHands, detailSigns, automaticPrediction, videoSigns, markLeadHand }; }

    public override string[] getToggleSettingsLabels()
    {
        return new string[] {   "Rotate Through Signs",
                                "Show camera",
                                "Detailed Hand Models",
                                "Detailed Signs",
                                "Automatic Prediction",
                                "Video Signs",
                                "Mark Lead Hand"};
    }

    public override void setSetting(int settingIndex, bool isOn)
    {
        switch (settingIndex)
        {
            case 0:
                signRotationIsOn = isOn; break;

            case 1:
                showCamera = isOn; break;

            case 2:
                detailedHands = isOn; break;

            case 3:
                detailSigns = isOn; break;

            case 4:
                automaticPrediction = isOn; break;

            case 5:
                videoSigns = isOn; break;

            case 6:
                markLeadHand = isOn; break;

        }
    }
    //>><<

    //:::://    iNCREMENT SETTINGS FUNCTIONS    //:::://        
    public override float[] getIncrementValues() {
        float[] incrementValues = new float[2 + SignsMinConfidenceValues.Length];
        incrementValues[0] = countDownTimer;
        incrementValues[1] = samplesBeforeRotation;
        SignsMinConfidenceValues.CopyTo(incrementValues, 2);
        for (int i = 2; i < incrementValues.Length; i++)
        {
            incrementValues[i] *= 100;
        }
        return incrementValues;
        //return new float[] { countDownTimer, samplesBeforeRotation };
    }

    public override string[] getIncrementSettingsLabels()
    {
        string[] incrementValueLabels = new string[2 + alphabetSigns.Length];
        incrementValueLabels[0] = "Count down timer (seconds)";
        incrementValueLabels[1] = "# Recordings before rotation";
        alphabetSigns.CopyTo(incrementValueLabels, 2);
        return incrementValueLabels;
        //return new string[] {   "Count down timer (seconds)", "# Recordings before rotation"};
    }
    public override void setSetting(int settingIndex, float value)
    {
        switch (settingIndex)
        {
            case 0:
                countDownTimer = value; break;

            case 1:
                samplesBeforeRotation = value; break;

            default:
                Debug.Log("Changed Setting");
                SignsMinConfidenceValues[settingIndex - 2] = value / 100; break;
        }
    }
    //::::// 

    // Load from JSON Settings
    public override void save()
    {
        string dataToJson = JsonUtility.ToJson(this);
        string filePath = Path.Combine(getPath(), "RecSettings.json");
        File.WriteAllText(filePath, dataToJson);
    }

    public override UserSettings load()
    {
        string filePath = Path.Combine(getPath(), "RecSettings.json");

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            RecUserSettings uSettings = JsonUtility.FromJson<RecUserSettings>(dataAsJson);
            return uSettings;
        }
        else
        {
            RecUserSettings uSettings = new RecUserSettings();
            uSettings.countDownTimer = 3.0f;
            uSettings.numPredictionsPerSecond = 30;
            uSettings.samplesBeforeRotation = 1;
            uSettings.detailedHands = true;
            uSettings.videoSigns = false;
            uSettings.automaticPrediction = true;
            uSettings.SignsMinConfidenceValues = new float[] {      0.94f, 0.99f, 0.96f, 0.95f, 0.98f, 0.99f, 0.91f, 0.96f, 0.99f, 0.91f,
                                                                            0.87f, 0.99f, 0.67f, 0.57f, 0.97f, 0.87f, 0.79f, 0.71f, 0.61f, 0.78f,
                                                                            0.85f, 0.94f, 0.98f, 0.91f, 0.97f, 0.90f};
            return uSettings;
        }
    }
}

