﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class TrainUserSettings : UserSettings
{
    //##//  INCREMENT SETTINGS  //##//
    public float countDownTimer;        // index 0
    public float samplesBeforeRotation;   // index 1

    public float numSamplesPerSecond;
    //##//

    //^^ TOGGLE SETTINGS VARS//^^ 
    public bool signRotationIsOn;   // index 0 
    public bool showCamera;         // index 1
    public bool detailedHands;      // index 2
    public bool videoSigns;         // index 3
    public bool spellNewLetter;
    public bool markLeadHand;
    //^^ 

    //>><< TOGGLE SETTINGS FUNCTIONS//>><<
    public override bool[] getToggleValues() { return new bool[] { signRotationIsOn, showCamera, detailedHands, videoSigns, markLeadHand, spellNewLetter}; }

    public override string[] getToggleSettingsLabels() {
        return new string[] {   "Rotate Through Signs",
                                "Show Camera",
                                "Detailed Hand Models",
                                "Video Signs",
                                "Mark Lead Hand",
                                "Spell New Letter"
                            };
    }

    public override void setSetting(int settingIndex, bool isOn) {
        switch (settingIndex)
        {
            case 0:
                signRotationIsOn = isOn; break;

            case 1:
                showCamera = isOn; break;

            case 2:
                detailedHands = isOn; break;

            case 3:
                videoSigns = isOn; break;

            case 4:
                markLeadHand = isOn; break;

            case 5:
                spellNewLetter = isOn; break;

        }
    }
    //>><<

    //:::://    iNCREMENT SETTINGS FUNCTIONS    //:::://        
    public override float[] getIncrementValues() { return new float[] { countDownTimer, samplesBeforeRotation }; }

    public override string[] getIncrementSettingsLabels() {
        return new string[] {   "Count down timer (seconds)",
                                "# Recordings before rotation"};
    }
    public override void setSetting(int settingIndex, float value) {
        switch (settingIndex)
        {
            case 0:
                countDownTimer = value; break;

            case 1:
                samplesBeforeRotation = value; break;

        }
    }
    //::::// 

    // Load from JSON Settings
    public override void save(){
        string dataToJson = JsonUtility.ToJson(this);
        string filePath = Path.Combine(getPath(), "TrainSettings.json");
        File.WriteAllText(filePath, dataToJson);
    }

    public override UserSettings load()
    {
        string filePath = Path.Combine(getPath(), "TrainSettings.json");

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            TrainUserSettings uSettings = JsonUtility.FromJson<TrainUserSettings>(dataAsJson);
            return uSettings;
        }
        else
        {
            TrainUserSettings uSettings = new TrainUserSettings();
            uSettings.countDownTimer = 3.0f;
            uSettings.numSamplesPerSecond = 30;
            uSettings.samplesBeforeRotation = 1;
            uSettings.detailedHands = true;
            uSettings.videoSigns = false;
            return uSettings;
        }
    }
}
