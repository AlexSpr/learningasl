﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserSettings
{
    public virtual bool[]   getToggleValues() { return null; }
    public virtual string[] getToggleSettingsLabels() { return null; } 
    public virtual void     setSetting(int settingIndex, bool isOn) { }

    public virtual float[]  getIncrementValues() { return null; }
    public virtual string[] getIncrementSettingsLabels() { return null; }
    public virtual void     setSetting(int settingIndex, float value) { }

    public virtual void save() { }
    public virtual UserSettings load() { return null; }

    public static string getPath()
    {
        #if UNITY_EDITOR
                return Application.dataPath;
        #elif UNITY_ANDROID
                return Application.persistentDataPath;
        #elif UNITY_IPHONE
                return Application.persistentDataPath;
        #else
                return Application.dataPath;
        #endif
    }
}
