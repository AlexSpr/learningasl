﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecSettingsHandler : MonoBehaviour
{
    private GameObject settingsCanvas;

    private SettingsHandler<RecSignsController, RecUserSettings> SettingsHandler;   //sets what controller and settings the SettingsHandler uses

    void Start()
    {
        settingsCanvas = this.gameObject;   //gets Setting Canvas
        SettingsHandler = new SettingsHandler<RecSignsController, RecUserSettings>();   //creates new SettingsHandler
        settingsCanvas.SetActive(false);    //disables/hides settingsngsCanvas after initialization
    }

    void OnEnable() //on enable/unhidde 
    {
        if (SettingsHandler != null)       //if SettingsHandler is initiated
        {
            SettingsHandler.updateSettingsDisplay();    //update settings values 
        }
    }

    void OnDisable()
    {
        SettingsHandler.setSceneSettings(); //saves settings and updates Controller settings
    }
}
