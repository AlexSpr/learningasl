﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsHandler <T, U> where T: SceneController where U: UserSettings, new()
{
    private GameObject generalSettings;

    private T controller;
    private U currentSettings = new U();
    private U revertSettings;

    public SettingsHandler()
    {
        initializeSettingsHandler();
        updateSettingsDisplay();
        setupToggleSettings();
        setupIncrementSettings();
    }

    void initializeSettingsHandler()
    {
        controller = GameObject.FindObjectOfType<T>();
        generalSettings = GameObject.Find("settingsCanvas/blurPanel/settingsPanel/generalSettings");  
    }

    public void updateSettingsDisplay()
    {
        currentSettings = (U)currentSettings.load();
        revertSettings = currentSettings;
    }

    public void setSceneSettings()
    {
        currentSettings.save();
        controller.updateSceneSettings(currentSettings);
    }

    private void setupToggleSettings()  //creates toggle settings from prefab
    {
        bool[] toggleValues = currentSettings.getToggleValues();
        string[] toggleSettingsLabels = currentSettings.getToggleSettingsLabels();

        for (int i = 0; i < toggleSettingsLabels.Length; i++)
        {
            GameObject toggleSetting = GameObject.Instantiate(Resources.Load("Prefabs/toggleSetting")) as GameObject;
            toggleSetting.transform.SetParent(generalSettings.transform);
            toggleSetting.GetComponent<TogglePrefabHandler>().setToggleSetting(i, toggleValues[i], toggleSettingsLabels[i], toggleValueChange);
        }
    }

    private void setupIncrementSettings() //creates increment settings from prefab
    {
        float[] incrementValues = currentSettings.getIncrementValues();
        string[] incrementSettingLabels = currentSettings.getIncrementSettingsLabels();

        for (int i = 0; i < incrementSettingLabels.Length; i++)
        {
            GameObject incrementSetting = GameObject.Instantiate(Resources.Load("Prefabs/incrementSetting")) as GameObject;
            incrementSetting.transform.SetParent(generalSettings.transform);
            incrementSetting.GetComponent<IncrementPrefabHandler>().setIncrementSetting(i, incrementValues[i], incrementSettingLabels[i], incrementValueChange);
        }

    }

    void toggleValueChange(int settingIndex, bool isOn)
    {
        currentSettings.setSetting(settingIndex, isOn);
    }

    public void incrementValueChange(int settingIndex, float settingValue)
    {
        currentSettings.setSetting(settingIndex, settingValue);
    }
}
