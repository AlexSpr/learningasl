﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class TrainDataHandler
{
    //Path information for Pics, CSVs, and Sprites
    string appPath;
    string generalDataPath;
    string rawTextPath;
    string rawPicPath;

    string currentCSVSignPath = " ";
    string currentPicSignPath = " ";
    string currentSign = "";
    string seqNameAdd = "_Seq";
    string textFileType = ".csv";
    string picFileType = ".png";

    StringBuilder signSequence;
    bool newSequence = true;

    List<Texture2D> _imageTexturesList;
    int seqPicIndex = 0;
    bool seqHandIsRight = true;
    //Only used to crerate Header for CSV
    string[] fingers = new string[] {"Thumb","Index", "Middle", "Ring", "Pinky"};
    string[] fingerBones = new string[] { "Distal", "Intermediate", "Proximal", "Metacarpal" }; 
    string[] fingerTrackers = new string[] { "Top", "Base" };
    string[] boneCoordinates = new string[] { "x", "y", "z" };
    string[] handOrientation = new string[] { "HandPitch", "HandYaw", "HandRoll" };
    string[] armOrientation = new string[] { "ArmX", "ArmY", "ArmZ" };
    string[] userInfo = new string[] { "captureTime", "userName" };
    //Arrays holding number of Samples
    private int[] signCSVSampleNum = new int[36];
    private int[] signPicSampleNum = new int[36];

    private SignInfo signInfo = new SignInfo();

    public TrainDataHandler()
    {
        appPath = getPath();
        generalDataPath = appPath + "Data/";
        rawTextPath = generalDataPath + "Raw/Text/";
        rawPicPath  = generalDataPath + "Raw/Pics/";

        Directory.CreateDirectory(rawTextPath);
        Directory.CreateDirectory(rawPicPath);
        collectCSVSignsData(ref rawTextPath);
        collectPicSignsData(ref rawPicPath);

    }

    public void addSignToCSV(float[][][] fingerTipMatrix, float[][][] fingerBaseMatrix, float[] handOrientation, float[] armOrientation, string Sign, string captureTime, string currentUser)
    {
        currentSign = Sign;
        currentCSVSignPath = rawTextPath + Sign + textFileType;
        StringBuilder values = new StringBuilder();
        createValuesString(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation, ref Sign, ref captureTime, ref currentUser, ref values);
        checkforCSV(ref currentCSVSignPath);
        File.AppendAllText(currentCSVSignPath, values.ToString());
    }
 
    public void addSeqSampleForCSV(float[][][] fingerTipMatrix, float[][][] fingerBaseMatrix, float[] handOrientation, float[] armOrientation, string Sign, string captureTime, string currentUser)
    {
        if (newSequence)
        {
            newSequence = false;
            currentSign = Sign;
            signSequence = new StringBuilder();
        } else
        {
            Sign = " ";
        }

        createValuesString(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation, ref Sign, ref captureTime, ref currentUser, ref signSequence);

    }

    public void addSignSeqToCSV()
    {
        currentCSVSignPath = rawTextPath + currentSign + seqNameAdd + textFileType;
        checkforCSV(ref currentCSVSignPath);
        File.AppendAllText(currentCSVSignPath, signSequence.ToString());
        signSequence.Clear();
        newSequence = true;
    }

    public void resetSequenceSamples()
    {
        signSequence.Clear();
        newSequence = true;
        _imageTexturesList.Clear();
        seqPicIndex = 0;
    }

    public void addSignToPic(Texture2D imageTexture, string Sign, bool isHandRight, int SampleNum, string captureTime, string currentUser)
    {
        currentSign = Sign;
        if (!isHandRight)
        {
            imageTexture = flipTexture(imageTexture);
        }
        currentPicSignPath = rawPicPath + Sign; 
        Directory.CreateDirectory(currentPicSignPath);
        currentPicSignPath = currentPicSignPath + "/" + SampleNum.ToString() + "_" + captureTime + "_" + currentUser + picFileType;

        var bytes = imageTexture.EncodeToPNG();
        System.IO.File.WriteAllBytes(currentPicSignPath, bytes);
    }

    public void setSignSeq(List<Texture2D> imageTexturesList, string Sign, bool isHandRight, int SampleNum, string captureTime, string currentUser)
    {
        currentSign = Sign;
        currentPicSignPath = rawPicPath + Sign;
        currentPicSignPath = currentPicSignPath + "/" + SampleNum.ToString() + "_" + captureTime + "_" + currentUser; // + picFileType;
        Directory.CreateDirectory(currentPicSignPath);

        seqHandIsRight = isHandRight;
        _imageTexturesList = imageTexturesList;
    }

    public bool addOneSignSeqToPic()
    {
        if (seqPicIndex < _imageTexturesList.Count)
        {
            string currentImageIndexPath = currentPicSignPath + "/" + seqPicIndex.ToString() + picFileType;
            if (!seqHandIsRight)
            {
                _imageTexturesList[seqPicIndex] = flipTexture(_imageTexturesList[seqPicIndex]);
            }
            var bytes = _imageTexturesList[seqPicIndex].EncodeToPNG();
            System.IO.File.WriteAllBytes(currentImageIndexPath, bytes);
            seqPicIndex += 1;
            return false;
        } else
        {
            _imageTexturesList.Clear();
            seqPicIndex = 0;
            return true;
        }
    }
    
    public void addSignSeqToPic(List<Texture2D> imageTexturesList, string Sign, bool isHandRight, int SampleNum)
    {
        currentSign = Sign;

        currentPicSignPath = rawPicPath + Sign;
        currentPicSignPath = currentPicSignPath + "/" + SampleNum.ToString(); // + picFileType;
        Directory.CreateDirectory(currentPicSignPath);

        
        for (int index =  0;  index < imageTexturesList.Count; index++)
        {
            string currentImageIndexPath = currentPicSignPath + "/" + index.ToString() + picFileType;
            if (!isHandRight)
            {
                imageTexturesList[index] = flipTexture(imageTexturesList[index]);
            }
            var bytes = imageTexturesList[index].EncodeToPNG();
            System.IO.File.WriteAllBytes(currentImageIndexPath, bytes);
        }
        
    }
    
    Texture2D flipTexture(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height, TextureFormat.Alpha8, false, true);

        int xN = original.width;
        int yN = original.height;


        for (int i = 0; i < xN; i++)
        {
            for (int j = 0; j < yN; j++)
            {
                flipped.SetPixel(xN - i - 1, j, original.GetPixel(i, j));
            }
        }
        flipped.Apply();

        return flipped;
    }

    public void deleteLastSign()
    {
        deleteLastCSVSign();
        deleteLastPicSign();
        currentSign = "";
    }

    public void readCSV()
    {

    }

    public int[] getNumOfPicSamples()
    {
        return signPicSampleNum;
    }

    public int[] getNumberOfCSVSamples()
    {
        return signCSVSampleNum;
    }

    private void deleteLastPicSign()
    {
        if (currentPicSignPath != " ")
        {
            if (Directory.Exists(currentPicSignPath))
            {
                Directory.Delete(currentPicSignPath, true);
            }
            else
            {
                File.Delete(currentPicSignPath);
            }
        }
        currentPicSignPath = " ";
    }

    private void deleteLastCSVSign()
    {
        if (currentCSVSignPath != " ")
        {
            StringBuilder CSVlines = new StringBuilder();
            StringBuilder toBeAdded = new StringBuilder();

            var lineCount = File.ReadLines(currentCSVSignPath).Count();

            int counter = 0;
            foreach (var line in File.ReadLines(currentCSVSignPath))
            {
                if (counter == 0)
                {
                    CSVlines.Append(line);
                    CSVlines.Append(Environment.NewLine);
                }
                else if (counter < lineCount)
                {
                    string signID = line.Split(',')[0];
                    if (signID == currentSign && counter > 1)
                    {
                        CSVlines.Append(toBeAdded.ToString());
                        toBeAdded.Clear();
                        toBeAdded.Append(line);
                        toBeAdded.Append(Environment.NewLine);
                    }
                    else
                    {
                        toBeAdded.Append(line);
                        toBeAdded.Append(Environment.NewLine);
                    }

                }
                counter += 1;
            }

            File.WriteAllText(currentCSVSignPath, CSVlines.ToString());
            currentCSVSignPath = " ";
        }
    }

    private void createValuesString(ref float[][][] fingerTipMatrix, ref float[][][] fingerBaseMatrix, ref float[] handOrientation, ref float[] armOrientation, ref string Sign, ref string captureTime, ref string userName, ref StringBuilder values)
    {
        values.Append(Sign);
        values.Append(",");
        flattenArrayToString(ref fingerTipMatrix, ref values);
        flattenArrayToString(ref fingerBaseMatrix, ref values);
        flattenArrayToString(ref handOrientation, ref values);
        flattenArrayToString(ref armOrientation, ref values);
        values.Append(captureTime);
        values.Append(",");
        values.Append(userName);
        values.Append(Environment.NewLine);
    }

    private void flattenArrayToString(ref float[][][] multiArrray, ref StringBuilder values)
    {
        for(int i = 0; i < multiArrray.Length; i++)
        {
            flattenArrayToString(ref multiArrray[i], ref values);
        }
    }

    private void flattenArrayToString(ref float[][] multiArrray, ref StringBuilder values)
    {
        for (int i = 0; i < multiArrray.Length; i++)
        {
            flattenArrayToString(ref multiArrray[i], ref values);
        }
    }

    private void flattenArrayToString(ref float[] arrray, ref StringBuilder values)
    {
        for (int i = 0; i < arrray.Length; i++)
        {
            values.Append(arrray[i]);
            values.Append(",");
        }
    }

    private void collectPicSignsData(ref string folderPath)
    {
        foreach (string signFolder in Directory.GetDirectories(folderPath))
        {
            string picSign = Path.GetFileNameWithoutExtension(signFolder);
            int signIndex = Array.IndexOf(signInfo.Signs, picSign);
            //signPICSampleNum[signIndex] = getNumberOfSamplesInCSV(file, ref csvSign);

            foreach (string file in Directory.GetFiles(signFolder, "*" + picFileType))
            {
                if (signIndex >= 0)
                {
                    signPicSampleNum[signIndex] += 1;
                }
            }

            foreach (string seqFolder in Directory.GetDirectories(signFolder))
            {
                if (signIndex >= 0)
                {
                    signPicSampleNum[signIndex] += 1;
                }
            }
        }
    }

    private void collectCSVSignsData(ref string folderPath)
    {
        foreach (string file in Directory.GetFiles(folderPath, "*" + textFileType))
        {
            string csvSign = Path.GetFileNameWithoutExtension(file).Substring(0, 1);
            int signIndex = Array.IndexOf(signInfo.Signs, csvSign);
            if (signIndex >= 0)
            {
                signCSVSampleNum[signIndex] += getNumberOfSamplesInCSV(file, ref csvSign);
            }
            
        }
    }

    private int getNumberOfSamplesInCSV(string file, ref string csvSign)
    {
        int nummberOfSamples = 0;
        using (var reader = new StreamReader(file))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                if(values[0] == csvSign)
                {
                    nummberOfSamples += 1;
                }
            }
        }
        return nummberOfSamples;
    }

    private void checkforCSV(ref string filePath)
    {
        if (!File.Exists(filePath))
        {
            StringBuilder fileHeader = new StringBuilder();

            fileHeader.Append("Sign,");

            for (int trackerIndex = 0; trackerIndex < fingerTrackers.Length; trackerIndex++)
            {
                for (int fingerIndex = 0; fingerIndex < fingers.Length; fingerIndex++)
                {
                    for (int boneIndex = 0; boneIndex < fingerBones.Length; boneIndex++)
                    {
                        for (int cordIndex = 0; cordIndex < boneCoordinates.Length; cordIndex++)
                        {
                            fileHeader.Append(fingers[fingerIndex]);
                            fileHeader.Append(fingerBones[boneIndex]);
                            fileHeader.Append(boneCoordinates[cordIndex]);
                            fileHeader.Append(fingerTrackers[trackerIndex]);
                            fileHeader.Append(",");
                        }
                    }
                }

            }

            for (int handOIndex = 0; handOIndex < handOrientation.Length; handOIndex++)
            {
                fileHeader.Append(handOrientation[handOIndex]);
                fileHeader.Append(",");
            }

            for (int armOIndex = 0; armOIndex < armOrientation.Length; armOIndex++)
            {
                fileHeader.Append(armOrientation[armOIndex]);
                fileHeader.Append(",");
            }

            for (int userInfoIndex = 0; userInfoIndex < userInfo.Length; userInfoIndex++)
            {
                fileHeader.Append(userInfo[userInfoIndex]);
                fileHeader.Append(",");
            }

            fileHeader.Append(Environment.NewLine);

            File.WriteAllText(filePath, fileHeader.ToString());
        }
    }

    private string getPath()
    {
    #if UNITY_EDITOR
            return Application.dataPath +"/";
    #elif UNITY_ANDROID
            return Application.persistentDataPath+"/";
    #elif UNITY_IPHONE
            return Application.persistentDataPath+"/";
    #else
            return Application.dataPath + "/";
    #endif
    }

}
