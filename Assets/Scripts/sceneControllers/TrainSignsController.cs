﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class TrainSignsController : SceneController {

    public GameObject instructionsText;
    public GameObject handInfoText;
    public GameObject currentSignText;
    public GameObject userNameText;
    public GameObject numOfSamplesText;
    public GameObject timerText;

    public GameObject settingsCanvas;
    public GameObject userLoginCanvas;

    private GameObject cameraOutputPanel;

    private GameObject capsuleHandModels;
    private GameObject riggedHandModels;

    //-  TRAINSIGNSCONTROLLER VARIABLES  //-
    private DateTime currentTime;
    private string currentTimeString;

    private string currentUser = "Unknown";

    private string currentSign;
    private string lastSign;

    private bool leadHandIsRight = false;

    private string lastSample;
    private int lastSampleIndex = int.MaxValue;

    private int currentSignIndex = int.MaxValue; 
    private int lastSignIndex = 0;

    private string input;
    private string SpaceKey = " ";

    private bool signSelected = false;  //check if sign Selected
    private bool spaceWasPressed = false;   //check if space is pressed and start timer
    private bool spaceIsPressed = false;    //check if space is currently pressed   
    private bool sequenceInitiated = false; //if space is pressed after count down ends set to true 
    private bool picSeqWasSaved = true; // is set to false while seq data is being saved
    //-

    //%%%// SEQ_DATA VARIABLES //%%%//
    List<Texture2D> outputTextureList = new List<Texture2D>();
    //%%%//

    /////   USER SETTINGS   /////
    private float countDownTimer = 3; //Count Down Timer in Seconds - 1 | Is also used for counting up during sequence recordings
    private float samplesBeforeRotation = 1;    // num of samples before rotating to next Sign 
    private float rotationCounter = 0;  // counter counting the number of samples collect of current Sign  
    public TrainUserSettings currentUserSettings = new TrainUserSettings();    // class loading settings
    /////

    //---// COLLECTED DATA INFO //---// 
    private SignInfo signInfo = new SignInfo();
    private int[] signCSVSampleNum = new int[36];
    private int[] signPicSampleNum = new int[36];
    //private string[] signSpritesPaths = new string[36];
    //---//

    HandTracker tracker;
    TrainDataHandler dataHandler;
    ImageRetriever imageRetriever;
    FlashCanvasHandler flashCanvas;
    
    // Use this for initialization
    void Start () {

        dataHandler = new TrainDataHandler();
        signCSVSampleNum = dataHandler.getNumberOfCSVSamples();
        signPicSampleNum = dataHandler.getNumOfPicSamples();

        tracker = GameObject.FindObjectOfType<HandTracker>();
        imageRetriever = GameObject.FindObjectOfType<ImageRetriever>();
        flashCanvas = GameObject.FindObjectOfType<FlashCanvasHandler>();
        cameraOutputPanel = GameObject.Find("cameraOutputPanel");

        capsuleHandModels = GameObject.Find("Capsule Hand Models");
        riggedHandModels = GameObject.Find("Rigged Hand Models");
        /*
        //Materials[] handMaterail riggedHandModels
        rightHandMesh = riggedHandModels.transform.Find("LoPoly Rigged Hand Right/LoPoly_Hand_Mesh_Right").gameObject;
        Debug.Log(rightHandMesh);
        Material[] mats = rightHandMesh.GetComponent<Renderer>().materials;
        Debug.Log(mats[1]);

        mats[1].SetFloat("_Outline", 0);
        */

        updateSceneSettings(currentUserSettings.load());

        capsuleHandModels.SetActive(!currentUserSettings.detailedHands);
        riggedHandModels.SetActive(currentUserSettings.detailedHands);

        
        if (currentUserSettings.signRotationIsOn)
        {
            setSign(0);
        }

        setName(currentUser);

    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(currentTime.ToString("yyyy-MM-dd HH-mm-ss"));
        if (!settingsCanvas.activeSelf && !userLoginCanvas.activeSelf) {
            leadHandIsRight = tracker.leadHandIsRight;
            updateHandDetectInfo(); //updates text that shows if hands are detected
            checkForKeyEvent();     //checks for keyboard input
            
            instructionsText.GetComponent<Text>().text = "Hello There1";
            if (signSelected && spaceWasPressed)
            {
                countDown();
            }

            updateInstructionText();

            if (!picSeqWasSaved)
            {
                picSeqWasSaved = dataHandler.addOneSignSeqToPic();
                if (picSeqWasSaved)
                {
                    outputTextureList.Clear();
                }
            }
        } else
        {
            resetRecordingVaribales();
        }
        Resources.UnloadUnusedAssets();
    }

    void countDown()
    {
        StringBuilder timerText = new StringBuilder();
        countDownTimer -= Time.deltaTime;

        if (tracker.handTracked)    // if hand is tracked record data 
        { 
            if (countDownTimer < 0 )    // if count down timer ends 
            {
                currentTime = DateTime.Now;
                currentTimeString = currentTime.ToString("yyyy-MM-dd HH-mm-ss");

                if (!spaceIsPressed)    // if space is not pressed 
                {

                    flashCanvas.createFlash(currentTimeString);

                    if (!sequenceInitiated) // if single sample data was recorded 
                    {
                        //flashCanvas.createFlash();  //create flash
                        // Collect Leap hand data
                        float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
                        float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
                        float[] handOrientation = tracker.getHandOrientation();
                        float[] armOrientation = tracker.getArmOrientation();
                        dataHandler.addSignToCSV(fingerTipMatrix, fingerBaseMatrix, handOrientation, armOrientation, currentSign, currentTimeString, currentUser);  // save hand data

                        bool recordedHandIsRight = tracker.leadHandIsRight;
                        Texture2D outputTexture = imageRetriever.getCurrentImageTexture();  // get image data
                        dataHandler.addSignToPic(outputTexture, currentSign, recordedHandIsRight, signPicSampleNum[currentSignIndex], currentTimeString, currentUser);  // save image data

                    }


                    else // at end of seq save collected sequence data 
                    {
                        //flashCanvas.createFlash();  //create flash

                        CancelInvoke(); // stop recording seq data 
                        bool recordedHandIsRight = tracker.leadHandIsRight;
                        dataHandler.addSignSeqToCSV();  // save Leap seq data
                        dataHandler.setSignSeq(outputTextureList, currentSign, recordedHandIsRight, signPicSampleNum[currentSignIndex], currentTimeString, currentUser);    // start saving pic seq
                        picSeqWasSaved = false; // pic seq has not bett fully saved 
                    }

                    signPicSampleNum[currentSignIndex] += 1;    // increase sample num of Leap sample
                    signCSVSampleNum[currentSignIndex] += 1;    // increase sample num of pic sample
                    lastSample = currentSign;   
                    lastSampleIndex = currentSignIndex;
                    updateCurrentSignInfo();

                    sequenceInitiated = false;
                    spaceWasPressed = false;
                    this.timerText.SetActive(false);
                    countDownTimer = currentUserSettings.countDownTimer;

                    if (currentUserSettings.signRotationIsOn)   // rotate to next sign if 
                    {
                        if (rotationCounter == samplesBeforeRotation - 1) // all sign samples have been collected
                        {
                            setSign(currentSignIndex + 1);
                            rotationCounter = 0;
                        }
                        else
                        {
                            rotationCounter += 1;
                        }
                    }
                }

                else //if space is pressed collect sequence data 
                {
                    if (!sequenceInitiated) //when collect sequence data is initiateed for the first time
                    {
                        float invokeFrequency = 1 / currentUserSettings.numSamplesPerSecond;    // get sample frequency 
                        InvokeRepeating("collectSqeuenceData", 0.0f, invokeFrequency);          // invoke Sequence data collection
                        sequenceInitiated = true;
                    }
                    this.timerText.GetComponent<Text>().text = (Math.Abs(countDownTimer)).ToString("n2");
                }
            }

            else // if count down timer is > 0 than update countdown timer 
            {
                this.timerText.GetComponent<Text>().text = ((int)(countDownTimer + 1)).ToString();
            }

        }

        else // if hand is not tracked anymore reset timer  
        {
            resetRecordingVaribales();
        }
    }
 
    private void collectSqeuenceData()
    {
        dataHandler.addSeqSampleForCSV(tracker.getFingerTipMatrix(), tracker.getFingerBaseMatrix(), tracker.getHandOrientation(), tracker.getArmOrientation(), currentSign, currentTimeString, currentUser);
        outputTextureList.Add(imageRetriever.getCurrentImageTexture());
    }

    //>><<//  SETTINGS FUNCTIONS  //>><<//
    public override void updateSceneSettings(UserSettings newSettings)
    {
        currentUserSettings = (TrainUserSettings)newSettings;
        countDownTimer = currentUserSettings.countDownTimer;
        samplesBeforeRotation = currentUserSettings.samplesBeforeRotation;
        
        checkChangedSettings();
    }
    
    private void checkChangedSettings()
    {
        if (cameraOutputPanel != null)
        {
            cameraOutputPanel.SetActive(currentUserSettings.showCamera);

        }
        if (currentUserSettings.signRotationIsOn)
        {
            setSign(currentSignIndex);
        }

        if (capsuleHandModels != null && riggedHandModels != null)
        {
            capsuleHandModels.SetActive(!currentUserSettings.detailedHands);
            riggedHandModels.SetActive(currentUserSettings.detailedHands);
        }

    }

    private void resetRecordingVaribales()
    {
        spaceWasPressed = false;
        if (sequenceInitiated)
        {
            CancelInvoke();
            dataHandler.resetSequenceSamples();
            outputTextureList.Clear();
            sequenceInitiated = false;
            picSeqWasSaved = true;
        }
        this.timerText.SetActive(false);
        countDownTimer = currentUserSettings.countDownTimer;
    }
    //>><<//    SETTINGS FUNCTIONS  END

    void checkForKeyEvent()
    {
        spaceIsPressed = Input.GetKey("space"); //if space is pressed set to true else false

        foreach (char c in Input.inputString) //goes through all keys pressed at call of function
        {
            string input = c.ToString().ToUpper();

            if (input == SpaceKey)
            {
                spaceWasPressed = true;
                timerText.SetActive(true);
            }
            else if (input.Contains("Alpha"))
            {
                input = input.Remove(0, 5);
            }
            else if (input.Contains("Keypad"))
            {
                input = input.Remove(0, 6);
            }

            if (signInfo.Signs.Contains(input))
            {
                if (!spaceWasPressed || !signSelected)
                {
                    lastSignIndex = currentSignIndex;
                    currentSignIndex = Array.IndexOf(signInfo.Signs, input);
                    
                    spaceWasPressed = false;
                    signSelected = true;

                    lastSign = currentSign;
                    currentSign = input;

                    updateCurrentSignInfo();
                }
            }
        }
    }

    public void deleteLastSample()
    {  
        if (lastSampleIndex != int.MaxValue)
        {
            dataHandler.deleteLastSign();
            signCSVSampleNum[lastSampleIndex] -= 1;
            signPicSampleNum[lastSampleIndex] -= 1;
            lastSampleIndex = int.MaxValue;
            updateCurrentSignInfo();
        }
    }

    private void setSign(int index)
    {
        lastSignIndex = currentSignIndex;
        if (index > signInfo.Signs.Length - 1) { 
            currentSignIndex = 0;//Array.IndexOf(signInfo.Signs, input);
        } 
        else if (index < 0)
        {
            currentSignIndex = signInfo.Signs.Length - 1;
        } 
        else
        {
            currentSignIndex = index;
        }
        spaceWasPressed = false;
        signSelected = true;

        lastSign = currentSign;
        currentSign = signInfo.Signs[currentSignIndex];

        updateCurrentSignInfo();
    }

    public bool isThereSampleToDelete()
    {
        if (lastSampleIndex != int.MaxValue)
        {
            return true;
        }
        return false;
    }

    public void setName(string newUser)
    {
        currentUser = newUser;
        this.userNameText.GetComponent<Text>().text = "User: " + currentUser;
    }

    public string getUserName()
    {
        return currentUser;
    }

    public string getLastSampleSign()
    {
        return lastSample;
    }
 
    public override int getCurrentSignIndex()
    {
        return currentSignIndex;
    }

    public override bool getleadHandIsRight()
    {
        return leadHandIsRight;
    }

    public override bool videoSignsOn()
    {
        return currentUserSettings.videoSigns;
    }

    public override bool getMarkLeadHand()
    {
        return currentUserSettings.markLeadHand;
    }

    public override bool spellNewLetter()
    {
        return currentUserSettings.spellNewLetter;
    }


    //^^^^//    TEXT FIELD FUNCTIONS    //^^^^//
    void updateCurrentSignInfo()
    {
        StringBuilder currentSignInfo = new StringBuilder();
        currentSignInfo.Append("Sign: " + currentSign);
        currentSignInfo.Append(Environment.NewLine);
        currentSignInfo.Append("CSV Samples: " + signCSVSampleNum[currentSignIndex].ToString());
        currentSignInfo.Append(Environment.NewLine);
        currentSignInfo.Append("Pic Samples: " + signPicSampleNum[currentSignIndex].ToString());
        currentSignText.GetComponent<Text>().text = currentSignInfo.ToString();
    }

    void updateInstructionText()
    {
        StringBuilder instructions = new StringBuilder();
        if (signSelected)
        {
            instructions.Append("Sign ");
            instructions.Append(currentSign);
            instructions.Append(" Selected");
            instructions.Append(Environment.NewLine);
            instructions.Append(Environment.NewLine);
            /*
            if (!spaceWasPressed)
            {
                instructions.Append("Press Space To Record Hand");
            }

            instructions.Append(Environment.NewLine);
            instructions.Append(Environment.NewLine);
            */
        }
        else
        {
            instructions.Append("Please Select Sign");
            instructions.Append(Environment.NewLine);
            instructions.Append(Environment.NewLine);
        }

        if (!tracker.handTracked)
        {
            instructions.Append("Hold Open Hand In Front Of Leap Camera");
        }
        instructionsText.GetComponent<Text>().text = instructions.ToString();

        numOfSamplesText.SetActive(currentUserSettings.signRotationIsOn);
        if (currentUserSettings.signRotationIsOn)
        {
            numOfSamplesText.GetComponent<Text>().text = (rotationCounter + 1).ToString() + " of " + samplesBeforeRotation.ToString() + " Samples";
        }
    }

    void updateHandDetectInfo()
    {
        StringBuilder handInfo = new StringBuilder();
        if (tracker.handTracked)
        {
            handInfo.Append("Hands detected: ");
            handInfo.Append(tracker.numberOfTrackedHands());
            handInfo.Append("\tLead Hand: ");
            if (tracker.leadHandIsRight) { 
                handInfo.Append("Right");
            } else
            {
                handInfo.Append("Left");
            }
            handInfo.Append("\t\tLead Hand Confidence: ");
            handInfo.Append(tracker.leadHandConfindece);
            handInfoText.GetComponent<Text>().text = handInfo.ToString();
        }
        else
        {
            handInfoText.GetComponent<Text>().text = "No Hands detected!";
        }

    }
    //^^^^//
}
