﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class viewHandDataController : MonoBehaviour {

    public GameObject handInfoText;
    public GameObject handInfoPanels;


    HandTracker tracker;

    //public handTracker tracker = new handTracker(); //object that tracks the hand movement
    //private bool handTracked; //bool that indicates if hands are tracked or not

    // Use this for initialization
    void Start () {
        tracker = GameObject.FindObjectOfType<HandTracker>();
        handInfoText.GetComponent<Text>().text = "Hello there";
	}
	
	// Update is called once per frame
	void Update () {
        updateHandDetectInfo();
    }
    
    void updateHandDetectInfo()
    {
        
        if (tracker.handTracked)
        {
            handInfoText.GetComponent<Text>().text = "Hands detected: " + tracker.numberOfTrackedHands().ToString();
            //populateHandCoordinatePanel();
            handInfoPanels.SetActive(true);
        }
        else
        {
            handInfoText.GetComponent<Text>().text = "No Hands detected!";
            handInfoPanels.SetActive(false);
        }
        
    }

}
