﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.Threading;
using UnityEngine.UI;

public class RecSignsController : SceneController
{
    public GameObject instructionsText;
    public GameObject handInfoText;
    public GameObject currentSignText;
    public GameObject numOfSamplesText;
    public GameObject timerText;

    public GameObject settingsCanvas;

    private GameObject cameraOutputPanel;

    private GameObject capsuleHandModels;
    private GameObject riggedHandModels;

    //-  TRAINSIGNSCONTROLLER VARIABLES  //-
    private string currentSign;
    private string lastSign;

    private bool leadHandIsRight = false;

    private string lastSample;
    private int lastSampleIndex = int.MaxValue;

    private int currentSignIndex = int.MaxValue;
    private int lastSignIndex = 0;

    private string predictedSignLeapNN = null;
    private int predictedSignIndexLeapNN = -1;
    private float[] leapNNPredictions;

    private string input;
    private string SpaceKey = " ";

    private bool signSelected = false;  //check if sign Selected
    private bool spaceWasPressed = false;   //check if space is pressed and start timer
    private bool spaceIsPressed = false;    //check if space is currently pressed   
    private bool sequenceInitiated = false; //if space is pressed after count down ends set to true 
    private bool picSeqWasSaved = true; // is set to false while seq data is being saved

    private bool automaticPrediciton = false;
    private bool endingAutomatic = false;

    private bool alreadyReset = true;
    //-

    //%%%// SEQ_DATA VARIABLES //%%%//
    List<Texture2D> outputTextureList = new List<Texture2D>();
    //%%%//

    /////   USER SETTINGS   /////
    private float countDownTimer = 3; //Count Down Timer in Seconds - 1 | Is also used for counting up during sequence recordings
    private float samplesBeforeRotation = 1;    // num of samples before rotating to next Sign 
    private float rotationCounter = 0;  // counter counting the number of samples collect of current Sign  
    private float cooldDownTime = 3;
    private float cooldDownTimer = 2;
    private float leeway = .05f;
    RecUserSettings currentUserSettings = new RecUserSettings();    // class loading settings
    /////

    //---// COLLECTED DATA INFO //---// 
    private SignInfo signInfo = new SignInfo();
    private int[] signCSVSampleNum = new int[36];
    private int[] signPicSampleNum = new int[36];
    //private string[] signSpritesPaths = new string[36];
    //---//

    HandTracker tracker;
    TrainDataHandler dataHandler;
    ImageRetriever imageRetriever;
    FlashCanvasHandler flashCanvas;
    LeapSeqNNv2 leapNN;
    BarChart barChart;

    Thread leapNNThread;
    // Start is called before the first frame update
    void Start()
    {
        dataHandler = new TrainDataHandler();
        signCSVSampleNum = dataHandler.getNumberOfCSVSamples();
        signPicSampleNum = dataHandler.getNumOfPicSamples();

        tracker = GameObject.FindObjectOfType<HandTracker>();
        imageRetriever = GameObject.FindObjectOfType<ImageRetriever>();
        flashCanvas = GameObject.FindObjectOfType<FlashCanvasHandler>();
        leapNN = transform.GetComponent<LeapSeqNNv2>();
        barChart = GameObject.FindObjectOfType<BarChart>();

        cameraOutputPanel = GameObject.Find("cameraOutputPanel");

        capsuleHandModels = GameObject.Find("Capsule Hand Models");
        riggedHandModels = GameObject.Find("Rigged Hand Models");


        updateSceneSettings(currentUserSettings.load());

        //capsuleHandModels.SetActive(!currentUserSettings.detailedHands);
        //riggedHandModels.SetActive(currentUserSettings.detailedHands);

        if (currentUserSettings.signRotationIsOn)
        {
            setSign(0);
        }
    }
   

    // Update is called once per frame
    void Update()
    {
        if (!settingsCanvas.activeSelf)
        {

            leadHandIsRight = tracker.leadHandIsRight;
            updateHandDetectInfo(); //updates text that shows if hands are detected
            checkForKeyEvent();     //checks for keyboard input

            instructionsText.GetComponent<Text>().text = "Hello There1";
            if (!automaticPrediciton && !leapNN.isAutomaticPredicition())
            {
                if (spaceWasPressed && !leapNN.predicitionAvailable())
                {
                    float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
                    float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
                    float[] handOrientation = tracker.getHandOrientation();
                    float[] armOrientation = tracker.getArmOrientation();

                    leapNN.updateLeapData(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
                    leapNNThread = new Thread(leapNN.continuousPrediction);
                    leapNNThread.Start();

                    spaceWasPressed = false;
                }

                if (leapNN.predicitionAvailable())
                {
                    leapNNPredictions = leapNN.getPrediction();
                    float maxValue = leapNNPredictions.Max();
                    int predictedSignIndexLeapNN = leapNNPredictions.ToList().IndexOf(maxValue);
                    //Debug.Log(predictedSignIndexLeapNN);
                    predictedSignLeapNN = signInfo.Signs[predictedSignIndexLeapNN];
                    barChart.updateBarChart(leapNNPredictions);
                    leapNNThread.Join();
                }
            }
            else
            {
                if (!leapNN.isAutomaticPredicition())
                {
                    float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
                    float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
                    float[] handOrientation = tracker.getHandOrientation();
                    float[] armOrientation = tracker.getArmOrientation();


                    float invokeFrequency = 1 / 30;    // get sample frequency 
                    InvokeRepeating("collectSqeuenceData", 0.0f, invokeFrequency);          // invoke Sequence data collection
                    sequenceInitiated = true;

                    leapNN.updateLeapData(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
                    leapNNThread = new Thread(leapNN.makePrediction);
                    leapNNThread.Start();
                }

                else if (!automaticPrediciton && !endingAutomatic)
                {
                    CancelInvoke("collectSqeuenceData");
                    endingAutomatic = true;
                    leapNNThread.Join();
                    leapNN.endAutomaticPredicition();
                }
                /*
                float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
                float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
                float[] handOrientation = tracker.getHandOrientation();
                float[] armOrientation = tracker.getArmOrientation();

                leapNN.updateLeapData(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
                leapNNThread = new Thread(leapNN.makePrediction);
                leapNNThread.Start();
                */
            }

            updateInstructionText();
            if (!picSeqWasSaved)
            {
                picSeqWasSaved = dataHandler.addOneSignSeqToPic();
                if (picSeqWasSaved)
                {
                    outputTextureList.Clear();
                }
            }
        }
        else
        {
            resetRecordingVaribales();
        }
        Resources.UnloadUnusedAssets();
    }

    
    private void collectSqeuenceData()
    {

        if (tracker.handTracked)
        {
            alreadyReset = false;
            if (leapNN.predicitionAvailable())
            {
                leapNNPredictions = leapNN.getPrediction();
                float maxValue = leapNNPredictions.Max();
                int predictedSignIndexLeapNN = leapNNPredictions.ToList().IndexOf(maxValue);
                //Debug.Log(predictedSignIndexLeapNN);
                //Debug.Log(signInfo.SignsMinConfidenceValues[predictedSignIndexLeapNN]);
                //Debug.Log(maxValue);
                if (spaceIsPressed)
                {
                    if (currentSignIndex == predictedSignIndexLeapNN && currentUserSettings.SignsMinConfidenceValues[predictedSignIndexLeapNN] <= maxValue)
                    {
                        predictedSignLeapNN = signInfo.Signs[predictedSignIndexLeapNN];
                        cooldDownTimer = cooldDownTime;
                    }
                }
                else
                {
                    if (currentUserSettings.SignsMinConfidenceValues[predictedSignIndexLeapNN] <= maxValue)
                    {
                        predictedSignLeapNN = signInfo.Signs[predictedSignIndexLeapNN];
                        cooldDownTimer = cooldDownTime;
                    }
                }

                /*
                if (currentUserSettings.SignsMinConfidenceValues[predictedSignIndexLeapNN]  <= maxValue)
                {
                    predictedSignLeapNN = signInfo.Signs[predictedSignIndexLeapNN];
                    cooldDownTimer = 2;
                    //leapNN.resetNNInput();
                }
                */

                /*
                else
                {
                    predictedSignLeapNN = null;
                }
                */
                barChart.updateBarChart(leapNNPredictions);
            }

            if (leapNN.needsNewData())
            {
                float[][][] fingerTipMatrix = tracker.getFingerTipMatrix();
                float[][][] fingerBaseMatrix = tracker.getFingerBaseMatrix();
                float[] handOrientation = tracker.getHandOrientation();
                float[] armOrientation = tracker.getArmOrientation();

                leapNN.updateLeapData(ref fingerTipMatrix, ref fingerBaseMatrix, ref handOrientation, ref armOrientation);
                leapNN.setData();
            }
        }


        else if (!alreadyReset)
        {
            leapNN.resetNNInput();
            alreadyReset = true;
        }

    }
    
    //>><<//  SETTINGS FUNCTIONS  //>><<//
    public override void updateSceneSettings(UserSettings newSettings)
    {
        currentUserSettings = (RecUserSettings)newSettings;
        countDownTimer = currentUserSettings.countDownTimer;
        samplesBeforeRotation = currentUserSettings.samplesBeforeRotation;

        
        checkChangedSettings();
    }

    private void checkChangedSettings()
    {
        if (cameraOutputPanel != null)
        {
            cameraOutputPanel.SetActive(currentUserSettings.showCamera);

        }
        if (currentUserSettings.signRotationIsOn)
        {
            setSign(currentSignIndex);
        }

        if (capsuleHandModels != null && riggedHandModels != null)
        {
            capsuleHandModels.SetActive(!currentUserSettings.detailedHands);
            riggedHandModels.SetActive(currentUserSettings.detailedHands);
        }

        automaticPrediciton = currentUserSettings.automaticPrediction;
        //Debug.Log(automaticPrediciton);

    }

    private void resetRecordingVaribales()
    {
        spaceWasPressed = false;
        if (sequenceInitiated)
        {
            ///CancelInvoke();
            //dataHandler.resetSequenceSamples();
            //outputTextureList.Clear();
            sequenceInitiated = false;
            //picSeqWasSaved = true;
        }
        this.timerText.SetActive(false);
        countDownTimer = currentUserSettings.countDownTimer;
    }
    //>><<//    SETTINGS FUNCTIONS  END

    void checkForKeyEvent()
    {
        spaceIsPressed = Input.GetKey("space"); //if space is pressed set to true else false
        //Debug.Log()
        foreach (char c in Input.inputString) //goes through all keys pressed at call of function
        {
            string input = c.ToString().ToUpper();

            if (input == SpaceKey)
            {
                //spaceWasPressed = true;
                timerText.SetActive(true);
            }
            else if (input.Contains("Alpha"))
            {
                input = input.Remove(0, 5);
            }
            else if (input.Contains("Keypad"))
            {
                input = input.Remove(0, 6);
            }

            if (signInfo.Signs.Contains(input))
            {
                if (!spaceWasPressed || !signSelected)
                {
                    lastSignIndex = currentSignIndex;
                    currentSignIndex = Array.IndexOf(signInfo.Signs, input);

                    spaceWasPressed = false;
                    signSelected = true;

                    lastSign = currentSign;
                    currentSign = input;

                    updateCurrentSignInfo();
                }
            }
        }
    }

    private void setSign(int index)
    {
        lastSignIndex = currentSignIndex;
        if (index > signInfo.Signs.Length - 1)
        {
            currentSignIndex = 0;//Array.IndexOf(signInfo.Signs, input);
        }
        else if (index < 0)
        {
            currentSignIndex = signInfo.Signs.Length - 1;
        }
        else
        {
            currentSignIndex = index;
        }
        spaceWasPressed = false;
        signSelected = true;

        lastSign = currentSign;
        currentSign = signInfo.Signs[currentSignIndex];

        updateCurrentSignInfo();
    }

    public string getLastSampleSign()
    {
        return lastSample;
    }

    public override int getCurrentSignIndex()
    {
        return currentSignIndex;
    }

    public override bool getleadHandIsRight()
    {
        return leadHandIsRight;
    }

    public override bool getMarkLeadHand()
    {
        return currentUserSettings.markLeadHand;
    }

    //^^^^//    TEXT FIELD FUNCTIONS    //^^^^//
    void updateCurrentSignInfo()
    {
        StringBuilder currentSignInfo = new StringBuilder();
        currentSignInfo.Append("Sign: " + currentSign);
        currentSignInfo.Append(Environment.NewLine);
        currentSignInfo.Append("CSV Samples: " + signCSVSampleNum[currentSignIndex].ToString());
        currentSignInfo.Append(Environment.NewLine);
        currentSignInfo.Append("Pic Samples: " + signPicSampleNum[currentSignIndex].ToString());
        currentSignText.GetComponent<Text>().text = currentSignInfo.ToString();
    }

    void updateInstructionText()
    {
        StringBuilder instructions = new StringBuilder();
        if (signSelected)
        {
            instructions.Append("Sign ");
            instructions.Append(currentSign);
            instructions.Append(" Selected");
            instructions.Append(Environment.NewLine);
            instructions.Append(Environment.NewLine);

            /*
            if (!spaceWasPressed)
            {
                instructions.Append("Press Space To Record Hand");
            }
            */
            //instructions.Append(Environment.NewLine);
            //instructions.Append(Environment.NewLine);

        }
        else
        {   
            /*
            instructions.Append("Press leter to  Sign");
            instructions.Append(Environment.NewLine);
            instructions.Append(Environment.NewLine);
            */
        }

        if (!tracker.handTracked)
        {
            if (predictedSignLeapNN == null)
            {
                instructions.Append("Hold Open Hand In Front Of Leap Camera");
            }
            /*else
            {
                instructions.Append("I saw a " + predictedSignLeapNN);
            }
            */
        } 
        if (predictedSignLeapNN != null)
        {
            instructions.Append("I saw a " + predictedSignLeapNN);
            cooldDownTimer -= Time.deltaTime;
            //Debug.Log(cooldDownTimer);
            if (cooldDownTimer < 0)
            {
                predictedSignLeapNN = null;
            }
        }
        
        instructionsText.GetComponent<Text>().text = instructions.ToString();

        numOfSamplesText.SetActive(currentUserSettings.signRotationIsOn);
        if (currentUserSettings.signRotationIsOn)
        {
            numOfSamplesText.GetComponent<Text>().text = " ";
        }
    }

    void updateHandDetectInfo()
    {
        StringBuilder handInfo = new StringBuilder();
        if (tracker.handTracked)
        {
            handInfo.Append("Hands detected: ");
            handInfo.Append(tracker.numberOfTrackedHands());
            handInfo.Append("\tLead Hand: ");
            if (tracker.leadHandIsRight)
            {
                handInfo.Append("Right");
            }
            else
            {
                handInfo.Append("Left");
            }
            handInfo.Append("\t\tLead Hand Confidence: ");
            handInfo.Append(tracker.leadHandConfindece);
            handInfoText.GetComponent<Text>().text = handInfo.ToString();
        }
        else
        {
            handInfoText.GetComponent<Text>().text = "No Hands detected!";
        }

    }

    public override bool videoSignsOn()
    {
        return currentUserSettings.videoSigns;
    }
    //^^^^//
}
