﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public virtual void updateSceneSettings(UserSettings userSettings) { }
    public virtual int getCurrentSignIndex() { return 0; }
    public virtual bool getleadHandIsRight() { return false; } 
    public virtual bool videoSignsOn() { return false; }
    public virtual bool getMarkLeadHand() { return false; }
    public virtual bool spellNewLetter() { return false;  }
}

