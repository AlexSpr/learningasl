﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfirmUserButtonHandler : MonoBehaviour
{
    TrainSignsController controller;
    userLoginCanvas userLogin;
    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.FindObjectOfType<TrainSignsController>();
        userLogin = GameObject.FindObjectOfType<userLoginCanvas>();
    }

    public void confirmAndClose(GameObject userLoginCanvas)
    {
        controller.setName(userLogin.getName());
        userLoginCanvas.SetActive(false);
    }
}
