﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonHandler : MonoBehaviour {

    public void changeScene(string text)
    {
        SceneManager.LoadScene(sceneName: text);
    }

    public void quitASL()
    {

        if (!Application.isEditor)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
        //Application.Quit();
    }


    //private GameObject settingsCanvas = null;
    public void toggleSettings(GameObject settingsCanvas)
    {
        
        if (settingsCanvas != null)
        {
            if (settingsCanvas.activeSelf)
            {
                settingsCanvas.SetActive(false);
            } else
            {
                settingsCanvas.SetActive(true);
            }
       
        }
        
    }
}
