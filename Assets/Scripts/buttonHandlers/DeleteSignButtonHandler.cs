﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DeleteSignButtonHandler : MonoBehaviour {

    Text buttonText; 
    TrainSignsController controller;
    private bool newSignToDelete = false;
    // Use this for initialization
    void Start () {
        controller = GameObject.FindObjectOfType<TrainSignsController>();
        buttonText = GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (controller.isThereSampleToDelete())
        {
            buttonText.text = "Delete Last Sign: " + controller.getLastSampleSign();
            newSignToDelete = true;
        } else
        {
            if (newSignToDelete)
            {
                buttonText.text = "No Sign To Delete";
                newSignToDelete = false;
            }
        }

    }

    public void deleteLastSign()
    {
        //buttonText.text = "Delete";
        controller.deleteLastSample();
    }


}
