﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showPanelButtonHandler : MonoBehaviour {

    public GameObject Panel;
    bool buttonPressed = false;

    void Start()
    {
        Panel.SetActive(false);
    }

    public void showPanel()
    {
        buttonPressed = !buttonPressed;
        if (buttonPressed)
        {
            Panel.SetActive(true);
        } else
        {
            Panel.SetActive(false);
        }
    }
}
