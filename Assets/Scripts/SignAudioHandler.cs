﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SignAudioHandler : MonoBehaviour
{
    public AudioSource audioSource;

    private string audioPath = "AudioSigns";
    private bool spellNewLetter = false;

    private int currentSign = int.MaxValue;

    private string[] audioSignNames = new string[36];

    SceneController sceneController;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.FindObjectOfType<SceneController>();
        getAudioSignsFromResources();
    }

    // Update is called once per frame
    void Update()
    {
        spellNewLetter = sceneController.spellNewLetter();
        if (spellNewLetter)
        {
            int signIndex = sceneController.getCurrentSignIndex();
            if (currentSign != signIndex)
            {
                currentSign = signIndex;
                audioSource.clip = loadAudio();
                audioSource.Play();
            }
        }
    }

    private void playSignAudio()
    {

    }

    private void getAudioSignsFromResources()
    {
        UnityEngine.Object[] audioSigns;
        audioSigns = Resources.LoadAll(audioPath, typeof(AudioClip));
        foreach (var audio in audioSigns)
        {
            string audioName = audio.name;
            int signIndex = int.Parse(audioName.Split('_').First());
            audioSignNames[signIndex] = audioName;
        }
    }

    private AudioClip loadAudio()
    {
        return Resources.Load<AudioClip>("AudioSigns/" + audioSignNames[currentSign]);
    }
}
